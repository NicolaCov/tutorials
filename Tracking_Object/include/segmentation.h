#include "removeTable.h"
#include "super_voxels.h"
#include "mergeAdjacentSuperVoxel.h"
#include "clusterObjects.h"

struct Option_seg 
{
	//paraemters for the table plane estmation
	double th_ransac;

	//---------- parameters for the super voxel algorithm -----------
	bool use_transform;
	float seed_resolution;
	float voxel_resolution;
	float color_importance;
	float spatial_importance;
	float normal_importance;
 
	bool visualization; //visualize super voxels and their interconnections
	bool view_normals;


	// parameters for the clustering of voxel in facets
	float th_normals;
 	float th_centroid;
 	bool visualize_adjacency_facets_map;
	bool visualize_centroid_and_normals;


	//clustering of facets into objects
	bool visualize_objects;
};

void segmentation(PointCloudT::Ptr cloud, Objects & objs, Option_seg & opt);
