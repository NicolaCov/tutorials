#include <pcl/io/pcd_io.h>
#include <pcl/sample_consensus/method_types.h>
#include <pcl/sample_consensus/model_types.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/surface/convex_hull.h>
#include <pcl/segmentation/extract_polygonal_prism_data.h>
#include <pcl/segmentation/supervoxel_clustering.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <pcl/visualization/point_cloud_color_handlers.h> //to choose a random color
#include <Eigen/Dense>
#include <map>
#include <pcl/common/centroid.h>

#include <iostream>

#include <pcl/kdtree/kdtree_flann.h>


#include "mergeAdjacentSuperVoxel.h"

//this header file contains funtion to cluster the facets into objects

/*
typedef pcl::PointXYZRGBA PointT;
typedef pcl::PointCloud<PointT>::Ptr PointTCloud;
typedef pcl::PointNormal PointNT;
typedef pcl::PointCloud<PointNT> PointNCloudT;
typedef std::vector<uint32_t> Facet;
typedef std::vector<Facet > Facets;


struct Facet_supervoxel
{
	PointTCloud voxels;
	pcl::Normal normal;
	PointT centroid;
};*/

struct Object
{
	pcl::PointCloud<PointT>::Ptr points;
};

typedef std::vector<Object> Objects;
// --------- utilities functions -------------

bool visitedLabel_co(std::vector<uint32_t> visited, uint32_t label);

//convexity between centroids
bool convexity(uint32_t l1, uint32_t l2, std::vector<Facet_supervoxel> &facets);

//convexity between centroid of the current facet and the enarest point of the adjacent facet
bool convexity2(uint32_t l1, uint32_t l2, std::vector<Facet_supervoxel> &facets);


// This method is a recursive method that merge togheter adjacent facet.
void mergeFacetIT(	Object & obj,Objects & objs, std::multimap<uint32_t, uint32_t> & facets_adjacency,
					std::vector<uint32_t> & visited,std::multimap<uint32_t,uint32_t> & objects_map,
					std::vector<Facet_supervoxel> &facets, uint32_t label);

void visualizeObjects(Objects &objs);
//void visualizeObjects(std::vector<Facet_supervoxel>  & objs);

// ----------- Main Function -----------------
void clusterObject(	std::multimap<uint32_t, uint32_t> & facets_adjacency, 
					std::vector<Facet_supervoxel> &facets, Facets &facets_lbl,
					Objects & objs);

