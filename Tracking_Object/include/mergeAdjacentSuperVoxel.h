#ifndef LIST_H_   	//include guard
#define LIST_H_		//include guard

// List.h code


#include <pcl/io/pcd_io.h>
#include <pcl/sample_consensus/method_types.h>
#include <pcl/sample_consensus/model_types.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/surface/convex_hull.h>
#include <pcl/segmentation/extract_polygonal_prism_data.h>
#include <pcl/segmentation/supervoxel_clustering.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <pcl/visualization/point_cloud_color_handlers.h> //to choose a random color
#include <Eigen/Dense>
#include <map>
#include <pcl/common/centroid.h>

#include <iostream>

#include <vtkPolyLine.h>



typedef pcl::PointXYZRGBA PointT;
typedef pcl::PointCloud<PointT> PointCloudT;
typedef pcl::PointNormal PointNT;
typedef pcl::PointCloud<PointNT> PointNCloudT;
typedef std::vector<uint32_t> Facet;
typedef std::vector<Facet > Facets;

struct Facet_supervoxel
{
	pcl::PointCloud<PointT>::Ptr voxels;
	pcl::Normal normal;
	PointT centroid;
};


// --------------utilites Methods--------------

// perform dot product of n1 by n2= n1 . n2 
// It basically returns the cos of the angle between the 2 normals
// Method called by getFacetsByCommonOrientations & getFacetsByCommonNormalIT
float dotProduct(pcl::Normal n1, pcl::Normal n2);

// Method that check id two voxel are in the same surface. It basically check that they have a similar normal,
// with the dotPorduct function and that the dot product between the vector defiend as the difference between 
// their centroid and the average of their normals are othogonal, up to a threhsodls.
// th1 = threshold to decide if two adjacent voxel have a similar normal, 
// th2 = threshold to decide if their averaged normal is orthogonal to the vector of difference between centroids
// sv1 and sv2 are used with the centroid, while to check the similarity between normals we use facet_voxel and sv2
bool voxelSameSurface( Facet_supervoxel & facet_voxel, pcl::Supervoxel<PointT>::Ptr sv1, pcl::Supervoxel<PointT>::Ptr sv2, float th1 , float th2);

// Method that updates the normal of the facet as average of the supervoxels
// that currently belong to that facet. It is called by the getFacetsByCommonNormalIT
void updateFacetNormal(Facet_supervoxel & facet, Facet & facet_labels,std::map <uint32_t, pcl::Supervoxel<PointT>::Ptr > supervoxel_clusters);

// This method estimate the centroids of all the facets
void estimateCentroid(std::vector<Facet_supervoxel> & facets);

//This function wants to fix the normal to the curvatures surface, in order to have convex adjacent surfaces.
//Remark: This method is not good for that because there are some surfaces that are combination of two parallel flat surfaces
void fixNoFlatSurfacesNormal(std::vector<Facet_supervoxel> & facets);

//This method checks if the supervoxel with label "label" has already been visited, it returns false if it
// is the first time that  we work with that supervoxel, true if not. 
// Method called by getFacetsByCommonOrientations & getFacetsByCommonNormalIT
bool visitedLabel(std::vector<uint32_t> visited, uint32_t label);

// This method is a recursive method that let to construct a single facet keeping the record of the visited
// super voxel - Method called by getFacetsByCommonOrientations 
// visited [in-output]
// facets_map [input]
void facetConstruct(std::vector<uint32_t> & visited, 
					std::multimap<uint32_t, uint32_t> & facets_map,
					uint32_t supervoxel_label,
					Facet & curr_facet);

//This method is a recursive one that let to construt a single facet keeping the record of the added supervoxels
// It is called by getFacetsByCommonNormalIT
void facetConstructIT(	std::vector<uint32_t> & visited, uint32_t supervoxel_label,
						pcl::Supervoxel<PointT>::Ptr supervoxel,
						std::multimap<uint32_t, uint32_t> supervoxel_adjacency,
						std::map <uint32_t, pcl::Supervoxel<PointT>::Ptr > supervoxel_clusters,
					    Facets & facets_lbl, //facets label
					    Facet & facet_lbl,
					    float th, float th2, Facet_supervoxel & facet_voxel);

// This method gives a map similar to the one of supervoxels, but it simply shows the connection between facets.
// It is called by the getFacetsByCommonNormalIT
//void getFacetsAdjacency(std::vector<Facet_supervoxel> facets);

// This method check if the supervoxel with label "label" belongs to the facet defined by the clustering of supervoxels 
// indicated in "facet_lbl"
bool sameFacet(uint32_t & label, Facet & facet_lbl);

// This method shows in the viewer the connection between the surfaces/facets
void displayFacetAdjacencyMap(	std::multimap<uint32_t, uint32_t> & facets_adjacency,
								std::vector<Facet_supervoxel> & facets,
								boost::shared_ptr<pcl::visualization::PCLVisualizer> & visualizer_);

void addFacetsConnectionsToViewer (	   PointT &supervoxel_center,
                                   PointCloudT::Ptr &adjacent_supervoxel_centers,
                                   std::string supervoxel_name,
                                   boost::shared_ptr<pcl::visualization::PCLVisualizer> & viewer);

// This method returns the value of the dissimilarity. It is called by the clusteringByConcaveness method.
float dissimilarityMeasure(pcl::Supervoxel<PointT>::Ptr sv1, pcl::Supervoxel<PointT>::Ptr sv2);
//--------------------------------------------------------------------------------

// Get facets regrouping adjacent super voxel that shares a similar orientation/normal 
// At the moment this just passes all the supervoxel and creates a new multimap adding the adjacen supervoxel
// with a similar orientation. 
// supervoxel_adjacency [input]
// supervoxel_clusters	[input]
// facets 				[output]
// th 					[input] from 0->1 threshold for construct the facets(higher it is more restrictive is the facet clustering)
// This method is the simplest implementation for this aim, it gives bad results in cluttered scenes, as worst as 
// small are the seeds size. It was implemented with the main aim to learn how to manage with the structures 
// of the supervoxels. 
// Until now this function simpyl diplays the results and returns the labels of supervoxels that belongs to each facet.
void getFacetsByCommonOrientations(	std::multimap<uint32_t, uint32_t> supervoxel_adjacency,
									std::map <uint32_t, pcl::Supervoxel<PointT>::Ptr > supervoxel_clusters,
									std::multimap<uint32_t, uint32_t> & facets_map,
									float th );

// This method does exactly the same of getFacetsByCommonOrientations but it also updates iteratively the normal
// of each surface it is clustering each time a new supervoxel is added to the surface. The normal of the facet is
// computed as the average of normal of the current added supervoxels
// Fix that the algorithm used is different in all the implementation ( the idea is not really different but the implementation yes)
// So "big" differences in the computational cost could be observed

void getFacetsByCommonNormalIT(	std::multimap<uint32_t, uint32_t> & supervoxel_adjacency,
								std::multimap<uint32_t, uint32_t> & facets_adjacency,
								std::vector<Facet_supervoxel> &facets,
								Facets &facets_lbl,
								std::map <uint32_t, pcl::Supervoxel<PointT>::Ptr > supervoxel_clusters,
								std::multimap<uint32_t, uint32_t> & facets_map,
								float th, float th2,
								bool visualize_adjacency_facets_map,
								bool visualize_centroid_and_normals);
//REMARKKKKK: the threshold for the centroid and normals is setted inside the recursive function at line 116 
// Nice Setup: th1 = 0.9 or 0.85 and th2= 0.5 and super voxel size 0.03 (computational expensice)




// This method construct recursively the objects clusters by connecting adjacent supervoxels
// using the concaveness among them as condition. 
// This method wants to be the one implemented by Sergey Alexandrov
// in his blog http://www.pointclouds.org/blog/tocs/alexandrov/index.php
// at the post Supervoxel Segmentation
void clusteringByConcaveness(	std::multimap<uint32_t, uint32_t> supervoxel_adjacency,
								std::map <uint32_t, pcl::Supervoxel<PointT>::Ptr > supervoxel_clusters);

#endif 
//include guard
