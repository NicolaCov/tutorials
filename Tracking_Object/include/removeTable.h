#include <pcl/io/pcd_io.h>
#include <pcl/sample_consensus/method_types.h>
#include <pcl/sample_consensus/model_types.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/surface/convex_hull.h>
#include <pcl/segmentation/extract_polygonal_prism_data.h>
 
#include <iostream>

typedef pcl::PointXYZRGBA PointT;
typedef pcl::PointCloud<PointT>::Ptr PointTCloud;
typedef pcl::PointNormal PointNT;
typedef pcl::PointCloud<PointNT> PointNCloudT;

void removeTable(PointTCloud cloud, double zmin, double zmax);