#include <pcl/console/parse.h>
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/io/pcd_io.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <pcl/segmentation/supervoxel_clustering.h>

#include <iostream>
#include <pcl/ModelCoefficients.h>
#include <pcl/io/pcd_io.h>
#include <pcl/sample_consensus/method_types.h>
#include <pcl/sample_consensus/model_types.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/filters/extract_indices.h>

//VTK include needed for drawing graph lines
#include <vtkPolyLine.h>

struct option_sv
{
	// parameters for the super voxel algorithm
	bool use_transform;
	float seed_resolution;
	float voxel_resolution;
	float color_importance;
	float spatial_importance;
	float normal_importance;

	bool visualization; //visualize super voxels and their interconnections
	bool view_normals;
};

 typedef pcl::PointXYZRGBA PointT;
 typedef pcl::PointCloud<PointT> PointCloudT;
 typedef pcl::PointNormal PointNT;
 typedef pcl::PointCloud<PointNT> PointNCloudT;
 typedef pcl::PointXYZL PointLT;
 typedef pcl::PointCloud<PointLT> PointLCloudT; 

void addSupervoxelConnectionsToViewer (PointT &supervoxel_center,
                                   PointCloudT &adjacent_supervoxel_centers,
                                   std::string supervoxel_name,
                                   boost::shared_ptr<pcl::visualization::PCLVisualizer> & viewer);

void VCCS(  pcl::SupervoxelClustering<PointT> & super, PointCloudT::Ptr cloud,
            std::map <uint32_t, pcl::Supervoxel<PointT>::Ptr > & supervoxel_clusters,
            std::multimap<uint32_t, uint32_t> & supervoxel_adjacency, option_sv & opt );
