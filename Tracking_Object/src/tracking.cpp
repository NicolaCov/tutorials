#include <pcl/features/normal_3d.h>

#include "segmentation.h"
#include <pcl/kdtree/kdtree_flann.h>
#include <pcl/segmentation/extract_polygonal_prism_data.h>
#include <pcl/filters/voxel_grid.h>

// This program try to "track" (here the tracking term is not sued properly) an object. We do an action on the object i-th
// and we then analize what have changed considering only that object. So the program subtract the no-considered objects from 
// from the point cloud. It is not working fine, there is some problem in voxel filter.
 
int
main (int argc, char ** argv)
{
   if (argc < 2)
   {
     pcl::console::print_error ("Syntax is: %s <pcd-file> \n "
                                 "--NT Dsables the single cloud transform \n"
                                 "-v <voxel resolution>\n-s <seed resolution>\n"
                                 "-c <color weight> \n-z <spatial weight> \n"
                                 "-n <normal_weight>\n"
                                 "-nv <visualizes super voxel normals>\n"
                                 "-sv <visualzies super voxels and interconecctions>\n"
                                 "-tn <set threshold for the normals, in extract facet step>\n"
                                 "-tc <set the threshold for the centroid of adjacent supervoxel>\n"
                                 "Suggestion: -s 0.03 -tc 0.4 -tn 0.8\n", argv[0]);
     return (1);
   }
 
  double th_ransac= 0.015;

  pcl::PointCloud<PointT>::Ptr cloud(new pcl::PointCloud<PointT>);
  pcl::console::print_highlight ("Loading point cloud...\n");
  if (pcl::io::loadPCDFile<PointT> (argv[1], *cloud))
  {
    pcl::console::print_error ("Error loading cloud file!\n");
    return (1);
  }
  std::cout << "pcd loaded\n";

  //-------------------- Setting the options -----------------------------
  Option_seg opt;
  opt.th_ransac = 0.015;

   opt.use_transform = ! pcl::console::find_switch (argc, argv, "--NT");
 
   opt.voxel_resolution = 0.008f;
   bool voxel_res_specified = pcl::console::find_switch (argc, argv, "-v");
   if (voxel_res_specified)
     pcl::console::parse (argc, argv, "-v", opt.voxel_resolution);
 
   opt.seed_resolution = 0.1f;
   bool seed_res_specified = pcl::console::find_switch (argc, argv, "-s");
   if (seed_res_specified)
     pcl::console::parse (argc, argv, "-s", opt.seed_resolution);

   opt.color_importance = 0.2f;
   if (pcl::console::find_switch (argc, argv, "-c"))
     pcl::console::parse (argc, argv, "-c", opt.color_importance);
 
   opt.spatial_importance = 0.4f;
   if (pcl::console::find_switch (argc, argv, "-z"))
     pcl::console::parse (argc, argv, "-z", opt.spatial_importance);
 
   opt.normal_importance = 1.0f;
   if (pcl::console::find_switch (argc, argv, "-n"))
     pcl::console::parse (argc, argv, "-n", opt.normal_importance);

   opt.th_normals = 0.8f;
   if (pcl::console::find_switch (argc, argv, "-tn"))
   {
     pcl::console::parse (argc, argv, "-tn", opt.th_normals);
   }

   opt.th_centroid = 0.5f;
   if (pcl::console::find_switch (argc, argv, "-tc"))
   {
     pcl::console::parse (argc, argv, "-tc", opt.th_centroid);
   }

   opt.view_normals = false;
   if (pcl::console::find_argument(argc,argv,"-nv"))
   {
      pcl::console::parse (argc, argv, "-sv", opt.view_normals);
   }

   opt.visualization = false;
   if (pcl::console::find_argument(argc,argv,"-sv"))
   {
      pcl::console::parse (argc, argv, "-sv", opt.visualization);
   }

   opt.visualize_adjacency_facets_map = false;
   opt.visualize_centroid_and_normals = false;
   opt.visualize_objects = true;
   // -------------------------------------------------------------------------

   PointCloudT::Ptr cloud_downsampled (new PointCloudT);
   cloud_downsampled->points = cloud->points;
   removeTable(cloud_downsampled, 0.03, 1.);

   pcl::VoxelGrid<pcl::PointXYZRGBA > sor;
   sor.setInputCloud (cloud_downsampled);
   sor.setLeafSize (opt.voxel_resolution, opt.voxel_resolution, opt.voxel_resolution);
   sor.filter (*cloud_downsampled);

   Objects objs;
   segmentation(cloud, objs, opt);
   std::cout << "Number of objects detected: " << objs.size() << std::endl;


   //--- "tracking" object ---------------------------------
   // since we have no indices of the point cloud we have to substract them, 
   // a way to do it is by KdTree. 
   //So let's create a Copy of Our filtered point cloud:
   

   PointCloudT::Ptr cloud2 (new PointCloudT);
   //Let's create the point cloud merging all the points of the objects
   for (int i = 0; i < objs.size(); ++i)
   {
     *(cloud2->points) += *(objs[i].points);
   }
/*

    pcl::KdTreeFLANN<pcl::PointXYZRGBA> kdtree;
    kdtree.setInputCloud (cloud2);    

    uint size_objs = 0;
    for (int i = 0; i < objs.size(); ++i)
    {
      size_objs += objs[i].points->size();
    }
    std::cout << "size cloud: " << cloud_downsampled->size() << "    size cloud2: " << size_objs << std::endl;
   uint K = 1;

    pcl::PointIndices::Ptr indices(new pcl::PointIndices);


   //Now, let's filter all the point from the 1all objects.
  for (uint obj_id = 0; obj_id < objs.size(); ++obj_id)
  {



    for (uint i = 0; i < objs[obj_id].points->size(); ++i)
    {
          std::vector<int> pointIdxNKNSearch(K);
          std::vector<float> pointNKNSquaredDistance(K);

          pcl::PointXYZRGBA searchPoint = objs[obj_id].points->at(i) ;


          if (kdtree.nearestKSearch (searchPoint, K, pointIdxNKNSearch, pointNKNSquaredDistance)>0)
          {
            indices->indices.push_back(pointIdxNKNSearch[0]);
          }
    } 

  }
 std::cout << "size indices: " << indices->indices.size() << std::endl;

    //Now that we have the indeces, we can filter the input cloud
    pcl::ExtractIndices<pcl::PointXYZRGBA> extract;
    extract.setInputCloud(cloud2);
    extract.setIndices(indices);
    extract.setNegative(true);
    extract.filter(*cloud2);

    boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer1 (new pcl::visualization::PCLVisualizer ("Original Downsampled CLoud"));
    viewer1->setBackgroundColor (0, 0, 0);
   
    viewer1->addPointCloud (cloud_downsampled, "Downsampled Cloud");
    viewer1->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE,2.0, "Downsampled Cloud");
    viewer1->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_OPACITY,0.95, "Downsampled Cloud");
    viewer1->addCoordinateSystem (0.5);


    while (!viewer1->wasStopped ())
    {
      viewer1 ->spinOnce (100);
    }
*/
    boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer2 (new pcl::visualization::PCLVisualizer ("Filtered CLoud"));
    viewer2->setBackgroundColor (0, 0, 0);
   
    viewer2->addPointCloud (cloud2, "Filtered Cloud");
    viewer2->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE,2.0, "Filtered Cloud");
    viewer2->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_OPACITY,0.95, "Filtered Cloud");
    viewer2->addCoordinateSystem (0.5);


    while (!viewer2->wasStopped ())
    {
      viewer2 ->spinOnce (100);
    }

    

   return (0);
 }