#include "segmentation.h"

void segmentation(PointCloudT::Ptr cloud, Objects & objs, Option_seg & opt)
{

	removeTable(cloud, 0.03, 1.);

	// super voxel 
	pcl::SupervoxelClustering<PointT> super (opt.voxel_resolution, opt.seed_resolution, opt.use_transform);
   	std::map <uint32_t, pcl::Supervoxel<PointT>::Ptr > supervoxel_clusters;
   	std::multimap<uint32_t, uint32_t> supervoxel_adjacency;
   	option_sv opt_sv;
   	opt_sv.use_transform = opt.use_transform;
   	opt_sv.seed_resolution = opt.seed_resolution;
	opt_sv.voxel_resolution = opt.voxel_resolution;
	opt_sv.color_importance = opt.color_importance;
	opt_sv.spatial_importance = opt.spatial_importance;
	opt_sv.normal_importance = opt.normal_importance;
	opt_sv.visualization = opt.visualization;
	opt_sv.view_normals = opt.view_normals;
   	VCCS(super, cloud, supervoxel_clusters, supervoxel_adjacency, opt_sv);

   	
   	//clustering of voxels in facets. We cluster all the super voxels accordingly to their orientation and their surface
   	std::multimap<uint32_t, uint32_t> facets_map; 
	std::multimap<uint32_t, uint32_t> facets_adjacency;
	std::vector<Facet_supervoxel> facets;
	Facets facets_lbl;
	getFacetsByCommonNormalIT( supervoxel_adjacency, facets_adjacency,facets, facets_lbl, supervoxel_clusters,
	facets_map, opt.th_normals , opt.th_centroid, opt.visualize_adjacency_facets_map,opt.visualize_centroid_and_normals);

   	clusterObject(facets_adjacency, facets,facets_lbl, objs);
   	if( opt.visualize_objects)
   		visualizeObjects(objs);

	return;
}