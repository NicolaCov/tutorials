#include "mergeAdjacentSuperVoxel.h"

float dotProduct(pcl::Normal n1, pcl::Normal n2)
{
   	Eigen::Vector4f p = n1.getNormalVector4fMap ();
	Eigen::Vector4f q = n2.getNormalVector4fMap ();
	//std::cout << " dot product: " << p.dot(q) << std::endl;
	return p.dot (q);
}

bool voxelSameSurface( 	Facet_supervoxel & facet_voxel, pcl::Supervoxel<PointT>::Ptr sv1,
						pcl::Supervoxel<PointT>::Ptr sv2, float th1 , float th2)
{
	if(dotProduct(facet_voxel.normal, sv2->normal_) >= th1) //th (0->1) threshold
	{
		//get centroids
		pcl::PointXYZRGBA c1 = sv1->centroid_;
		pcl::PointXYZRGBA c2 = sv2->centroid_;

		Eigen::Vector4f vec_centroid;
		vec_centroid << (c1.x - c2.x) , (c1.y - c2.y) , (c1.z - c2.z) , 0; 
		vec_centroid.normalize();

		pcl::Normal average_normal;
		average_normal.normal_x = (sv1->normal_.normal_x + sv2->normal_.normal_x)/2;
		average_normal.normal_y = (sv1->normal_.normal_y + sv2->normal_.normal_y)/2;
		average_normal.normal_z = (sv1->normal_.normal_z + sv2->normal_.normal_z)/2;
		Eigen::Vector4f average_normal_eig = average_normal.getNormalVector4fMap();
		average_normal_eig.normalize();

		if(vec_centroid.dot(average_normal_eig) <= fabs((float)th2))
			return true;

	}
	else
		return false;
}

void updateFacetNormal(Facet_supervoxel & facet, Facet & facet_labels,std::map <uint32_t, pcl::Supervoxel<PointT>::Ptr > supervoxel_clusters)
{
	//create the normal vector of the facet
	pcl::Normal normal;
	normal.normal_x = 0;
	normal.normal_y = 0;
	normal.normal_z = 0;
	for (Facet::iterator i = facet_labels.begin(); i != facet_labels.end(); ++i)
	{
		pcl::Normal supervoxel_normal = (supervoxel_clusters.at (*i))->normal_;
		//performing the SUM
		normal.normal_x += supervoxel_normal.normal_x; 
		normal.normal_y += supervoxel_normal.normal_y;
		normal.normal_z += supervoxel_normal.normal_z;
	}
	normal.normal_x = normal.normal_x / facet_labels.size(); //the size of facet_lbl is the number of the voxels
	normal.normal_y = normal.normal_y / facet_labels.size();
	normal.normal_z = normal.normal_z / facet_labels.size();
	facet.normal = normal;

	return;
}

void estimateCentroid(std::vector<Facet_supervoxel> & facets)
 {
 	for (std::vector<Facet_supervoxel>::iterator i = facets.begin(); i != facets.end(); ++i)
 	{
 		Eigen::Vector4f centroid_vec;
 		compute3DCentroid(*(i->voxels),centroid_vec);

 		PointT centroid;
 		centroid.x = centroid_vec[0];
 		centroid.y = centroid_vec[1];
 		centroid.z = centroid_vec[2];

 		i->centroid = centroid;
 	}

 	return;
 }

 void fixNoFlatSurfacesNormal(std::vector<Facet_supervoxel> & facets)
 {
 	for (std::vector<Facet_supervoxel>::iterator i = facets.begin(); i != facets.end(); ++i) //pass all the facets
 	{
 		//fit a plane for that facet
 		// Objects for storing the point clouds.
		pcl::PointCloud<pcl::PointXYZRGBA>::Ptr plane(new pcl::PointCloud<pcl::PointXYZRGBA>);
	 
		// Get the plane model, if present.
		pcl::ModelCoefficients::Ptr coefficients(new pcl::ModelCoefficients);
		pcl::SACSegmentation<pcl::PointXYZRGBA> segmentation;
		segmentation.setInputCloud(i->voxels);
		segmentation.setModelType(pcl::SACMODEL_PLANE);
		segmentation.setMethodType(pcl::SAC_RANSAC);
		segmentation.setDistanceThreshold(0.001);
		segmentation.setOptimizeCoefficients(true);
		pcl::PointIndices::Ptr planeIndices(new pcl::PointIndices);

		if (planeIndices->indices.size() != 0) //if we have found a plane that fit the facet
		{
			i->normal.normal_x = - i->normal.normal_x;
			i->normal.normal_y = - i->normal.normal_y;
			i->normal.normal_z = - i->normal.normal_z;
		}

	 }

 	return;
 }


bool visitedLabel(std::vector<uint32_t> visited, uint32_t label)
{
	for (std::vector<uint32_t>::iterator it = visited.begin(); it != visited.end(); it++)
		if( *it == label)
			return true;

	return false;
}

void facetConstruct(std::vector<uint32_t> & visited,
					std::multimap<uint32_t,uint32_t> & facets_map,
					uint32_t supervoxel_label,
					Facet & curr_facet)
{
	if(!visitedLabel(visited,supervoxel_label))
	{
		visited.push_back(supervoxel_label);
		curr_facet.push_back(supervoxel_label);
		std::multimap<uint32_t,uint32_t>::iterator adjacent_itr = facets_map.equal_range (supervoxel_label).first;
	    for ( ; adjacent_itr != facets_map.equal_range (supervoxel_label).second; ++adjacent_itr)
	    {	
	    	uint32_t ns_label = adjacent_itr->second;
	    	facetConstruct(visited,facets_map,ns_label,curr_facet);
	    }
	}
	return;
}

void facetConstructIT(	std::vector<uint32_t> & visited, uint32_t supervoxel_label,
						pcl::Supervoxel<PointT>::Ptr supervoxel,
						std::multimap<uint32_t, uint32_t> supervoxel_adjacency,
						std::map <uint32_t, pcl::Supervoxel<PointT>::Ptr > supervoxel_clusters,
					    Facets & facets_lbl, //facets label
					    Facet & facet_lbl,
					    float th, float th2, Facet_supervoxel & facet_voxel)
{
			//std::cout << " recursive fucntion   size of facet: " << facet_lbl.size() << "\n"	;		   	
		    //Now we need to iterate through the adjacent supervoxels and check if they have similar normals
	 		std::multimap<uint32_t,uint32_t>::iterator adjacent_itr = supervoxel_adjacency.equal_range (supervoxel_label).first;

		    for ( ; adjacent_itr!=supervoxel_adjacency.equal_range (supervoxel_label).second; ++adjacent_itr)
		    {
		    	uint32_t ns_label = adjacent_itr->second;
		    	//std::cout << " visiting supervoxel labeled as: " << ns_label << " from: " << supervoxel_label <<  std::endl;
		    	pcl::Supervoxel<PointT>::Ptr neighbor_supervoxel = supervoxel_clusters.at(ns_label);	
	   		    if(!visitedLabel(visited,ns_label))
	   		    	if(voxelSameSurface(facet_voxel,supervoxel,neighbor_supervoxel,th,th2))
			      	{
		      			*(facet_voxel.voxels) += *(neighbor_supervoxel->voxels_); //adds the supervoxel to the facet
		      			facet_lbl.push_back(ns_label);
		      			visited.push_back(ns_label);
		      			updateFacetNormal(facet_voxel, facet_lbl, supervoxel_clusters);//Update facet Normal;

			      		//facetConstructIT(visited, supervoxel_label, supervoxel, supervoxel_adjacency, supervoxel_clusters,
			      		//				facets_lbl, facet_lbl, th, facet_voxel);

			      		facetConstructIT(visited, ns_label, supervoxel, supervoxel_adjacency, supervoxel_clusters,
			      						facets_lbl, facet_lbl, th, th2, facet_voxel);
			      	}
		    }

	return;
}

bool sameFacet(uint32_t & label, Facet & facet_lbl)
{
	for (std::vector<uint32_t>::iterator i = facet_lbl.begin(); i != facet_lbl.end(); ++i)
	{
		if(*i == label)
			return true;
	}
	return false;
}


void addFacetsConnectionsToViewer (PointT &supervoxel_center,
                                   pcl::PointCloud<PointT> &adjacent_supervoxel_centers,
                                   std::string supervoxel_name,
                                   boost::shared_ptr<pcl::visualization::PCLVisualizer> & viewer)
{
   vtkSmartPointer<vtkPoints> points = vtkSmartPointer<vtkPoints>::New ();
   vtkSmartPointer<vtkCellArray> cells = vtkSmartPointer<vtkCellArray>::New ();
   vtkSmartPointer<vtkPolyLine> polyLine = vtkSmartPointer<vtkPolyLine>::New ();
 
   //Iterate through all adjacent points, and add a center point to adjacent point pair
   pcl::PointCloud<PointT>::iterator adjacent_itr = adjacent_supervoxel_centers.begin ();
   for ( ; adjacent_itr != adjacent_supervoxel_centers.end (); ++adjacent_itr)
   {
     points->InsertNextPoint (supervoxel_center.data);
     points->InsertNextPoint (adjacent_itr->data);
   }
   // Create a polydata to store everything in
   vtkSmartPointer<vtkPolyData> polyData = vtkSmartPointer<vtkPolyData>::New ();
   // Add the points to the dataset
   polyData->SetPoints (points);
   polyLine->GetPointIds  ()->SetNumberOfIds(points->GetNumberOfPoints ());
   for(unsigned int i = 0; i < points->GetNumberOfPoints (); i++)
     polyLine->GetPointIds ()->SetId (i,i);
   cells->InsertNextCell (polyLine);
   // Add the lines to the dataset
   polyData->SetLines (cells);
   viewer->addModelFromPolyData (polyData,supervoxel_name);
}

void displayFacetAdjacencyMap(	std::multimap<uint32_t, uint32_t> & facets_adjacency,
								std::vector<Facet_supervoxel> & facets,
								boost::shared_ptr<pcl::visualization::PCLVisualizer> & visualizer_)
{

	for (uint32_t i = 0; i < facets.size(); ++i) //for each facet
	{
			
		   pcl::PointCloud<PointT> adjacent_supervoxel_centers;
           std::multimap<uint32_t,uint32_t>::iterator adjacent_itr = facets_adjacency.equal_range (i).first;
		   for ( ; adjacent_itr!=facets_adjacency.equal_range (i).second; ++adjacent_itr)
		   {
		       adjacent_supervoxel_centers.push_back (facets[adjacent_itr->second].centroid);
           }
           char ss[8];
     	   sprintf(ss,"facet_%d",static_cast<int>(i));
     	   addFacetsConnectionsToViewer(facets[i].centroid,adjacent_supervoxel_centers,ss,visualizer_);

    }
	return;
}

float dissimilarityMeasure(pcl::Supervoxel<PointT>::Ptr sv1, pcl::Supervoxel<PointT>::Ptr sv2)
{
 	Eigen::Vector4f diff_centroid;
 	diff_centroid << sv1->centroid_.x - sv2->centroid_.x , sv1->centroid_.y - sv2->centroid_.y , sv1->centroid_.z - sv2->centroid_.z , 0.;  
 	Eigen::Vector4f p = sv1->normal_.getNormalVector4fMap ();
	Eigen::Vector4f q = sv2->normal_.getNormalVector4fMap ();

	if(diff_centroid.dot(p) < 0)
		return (1-fabs(p.dot(q)));
	else
		return 0;
}

void getFacetsByCommonOrientations(	std::multimap<uint32_t, uint32_t> supervoxel_adjacency,
									std::map <uint32_t, pcl::Supervoxel<PointT>::Ptr > supervoxel_clusters,
									std::multimap<uint32_t, uint32_t> & facets_map,
									float th)
{
	//float th = 0.6; //th rad low bound threshold

	//we now iterate through the supervoxel adjacency multimap
    std::multimap<uint32_t,uint32_t>::iterator label_itr = supervoxel_adjacency.begin ();
    std::cout << "supervoxel_adjacency_map size : " << supervoxel_adjacency.size() << "\n";
   	for ( ; label_itr != supervoxel_adjacency.end (); ) //this for passes through all the super voxels, we have to keep track what supervoxels have been visited
   	//basically we have to keep track of the label
    {

  		//First get the label
	    uint32_t supervoxel_label = label_itr->first;
	    //Now get the supervoxel corresponding to the label
	    pcl::Supervoxel<PointT>::Ptr supervoxel = supervoxel_clusters.at (supervoxel_label);

	    //Now we need to iterate through the adjacent supervoxels and check if they have similar normals
 		std::multimap<uint32_t,uint32_t>::iterator adjacent_itr = supervoxel_adjacency.equal_range (supervoxel_label).first;
 		bool insert = false;
	    for ( ; adjacent_itr!=supervoxel_adjacency.equal_range (supervoxel_label).second; ++adjacent_itr)
	    {
	    	pcl::Supervoxel<PointT>::Ptr neighbor_supervoxel = supervoxel_clusters.at (adjacent_itr->second);	
	    	if(dotProduct(supervoxel->normal_,neighbor_supervoxel->normal_) >= th) //th (0->1) threshold
	      	{
	      		// create a multimap (we could do that also as a binary tree, I guess that it will be fasterbecause avoids checking phase)
	      		uint32_t ns_label = adjacent_itr->second;
	      		facets_map.insert(std::pair<uint32_t, uint32_t>(supervoxel_label,ns_label));
	      		insert = true;
	      	}

	    }
	    if(!insert) //if we have no found any adjacent supervoxel with similar orientation add the map that connect it to itself
	    	facets_map.insert(std::pair<uint32_t, uint32_t>(supervoxel_label,supervoxel_label));

	    //Move iterator forward to next label
     	label_itr = supervoxel_adjacency.upper_bound (supervoxel_label);
    }
    std::cout << "facets_map size : " << facets_map.size() << "\n";
    //REMARK: facets_map.size() returns the number of elements in the second member. It is normal that it is less than the supervoxel_adjacency

    //now we have created the map of adjacent sueprvoxels accordingly to their similar orientation
    //We are going now to cluster them in facets, we will save the facets as vectors of labels.
	Facets facets;
	std::vector<uint32_t> visited; //vector of label to track what are the visitated labels
    std::multimap<uint32_t,uint32_t>::iterator facets_itr = facets_map.begin ();
    for ( ; facets_itr != supervoxel_adjacency.end (); ) //this for passes through all the super voxels, we have to keep track what supervoxels have been visited
    {

    	uint32_t supervoxel_label = facets_itr->first;
    	Facet curr_facet;
    	//perform a recursive search
    	facetConstruct(visited, facets_map, supervoxel_label,curr_facet);
    	if(curr_facet.size() > 0)
    		facets.push_back(curr_facet);
    	
    	facets_itr = supervoxel_adjacency.upper_bound (supervoxel_label);
    }
    std::cout << "Number of facets: " << facets.size() << std::endl;

	//Now let's visualize the result!
    //First create the visualizer
    boost::shared_ptr<pcl::visualization::PCLVisualizer> visualizer_ (new pcl::visualization::PCLVisualizer ("3D Viewer"));
   	visualizer_->setBackgroundColor (0, 0, 0);
    visualizer_->addCoordinateSystem (0.5);

   	int counter = 0; //counter that helps to count how many facets have been encountered
    for (std::vector<std::vector<uint32_t> >::iterator it = facets.begin(); it != facets.end(); ++it)
    {
    	//get a random color for each new facet
    	float r, g, b, min, max;
    	r = static_cast <float> (rand()) / static_cast <float> (RAND_MAX);
    	g = static_cast <float> (rand()) / static_cast <float> (RAND_MAX);
    	b = static_cast <float> (rand()) / static_cast <float> (RAND_MAX);

    	Facet curr_facet = *it;

    	//Now let's color all the point of each supervoxel of the current facet
    	for (std::vector<uint32_t>::iterator i = curr_facet.begin(); i != curr_facet.end(); ++i)
    	{
    		uint32_t supervoxel_label = *i;
		    //Now get the supervoxel corresponding to the label
		    pcl::Supervoxel<PointT>::Ptr supervoxel = supervoxel_clusters.at (supervoxel_label);
		    PointCloudT::Ptr voxels = supervoxel->voxels_;

			char str[30];
			sprintf(str,"line%d",counter);
		    visualizer_->addPointCloud<PointT> (voxels, str);
    		visualizer_->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_COLOR, r, g, b, str); 
		 
    		counter++;
    	}
    	
    }
    

    while (!visualizer_->wasStopped ())
   	{
     	visualizer_->spinOnce (100);
   	}

	return;
}

void getFacetsByCommonNormalIT(	std::multimap<uint32_t, uint32_t> & supervoxel_adjacency,
								std::multimap<uint32_t, uint32_t> & facets_adjacency,
								std::vector<Facet_supervoxel> &facets,
								Facets &facets_lbl,
								std::map <uint32_t, pcl::Supervoxel<PointT>::Ptr > supervoxel_clusters,
								std::multimap<uint32_t, uint32_t> & facets_map,
								float th, float th2,
								bool visualize_adjacency_facets_map,
								bool visualize_centroid_and_normals)
{
	//we now iterate through the supervoxel adjacency multimap and simultaneously we create the facets
    std::multimap<uint32_t,uint32_t>::iterator label_itr = supervoxel_adjacency.begin ();

    //In this case, we create the facets as a vector of supervoxels
    //std::vector<pcl::Supervoxel<PointT>::Ptr > facets;/

    //std::vector<pcl::Supervoxel<PointT>::Ptr >::iterator facets_itr = facets.begin (); //iterator of factes
	std::vector<uint32_t> visited; //vector of labels to track what supervoxels have been already added to a facet

    std::cout << "supervoxel_adjacency_map size : " << supervoxel_adjacency.size() << "\n";
   	for ( ; label_itr != supervoxel_adjacency.end (); ) //this for passes through all the super voxels, we have to keep track what supervoxels have been visited
   	//basically we have to keep track of the label
    {
    	//First get the label
	    uint32_t supervoxel_label = label_itr->first;
	    //In this case, we update the visited vector only when a supervoxel is added to a facet. So first,
    	//at each new sueprvoxel considered, we have to see if it has been already added to a facet.
    	// The same procedure have to be done also for the next deeper steps. 
	    if(!visitedLabel(visited,supervoxel_label)) //it is a new facet
	    {
 			visited.push_back(supervoxel_label);

	    	//Now get the supervoxel corresponding to the label
		    pcl::Supervoxel<PointT>::Ptr supervoxel = supervoxel_clusters.at (supervoxel_label);

		    //initialize facet
		    Facet_supervoxel facet_voxel;
		    facet_voxel.normal = supervoxel->normal_; 
		    facet_voxel.voxels = supervoxel->voxels_;

   	    	Facet facet_lbl; //labels of the supervoxel of the current facet
   	    	facet_lbl.push_back(supervoxel_label);

      		facetConstructIT(visited, supervoxel_label, supervoxel, supervoxel_adjacency, supervoxel_clusters,
      						facets_lbl, facet_lbl, th, th2, facet_voxel);

      		facets.push_back(facet_voxel);
   	    	facets_lbl.push_back(facet_lbl);

	    }

	    //Move iterator forward to next label
     	label_itr = supervoxel_adjacency.upper_bound (supervoxel_label);
    }
    std::cout << "Number of facets: " << facets.size() << std::endl;

   // fixNoFlatSurfacesNormal(facets);
    estimateCentroid(facets);

	//Now let's visualize the result!
    //First create the visualizer
    boost::shared_ptr<pcl::visualization::PCLVisualizer> visualizer_ (new pcl::visualization::PCLVisualizer ("3D Viewer"));
   	visualizer_->setBackgroundColor (0, 0, 0);
    visualizer_->addCoordinateSystem (0.5);


   	int counter = 0; //counter that helps to count how many facets have been encountered
    for (std::vector<Facet_supervoxel >::iterator it = facets.begin(); it != facets.end(); ++it)
    {
    	//get a random color for each new facet
    	float r, g, b, min, max;
    	r = static_cast <float> (rand()) / static_cast <float> (RAND_MAX);
    	g = static_cast <float> (rand()) / static_cast <float> (RAND_MAX);
    	b = static_cast <float> (rand()) / static_cast <float> (RAND_MAX);

    	char str[30];
		sprintf(str,"facet_%d",counter);
		
		visualizer_->addPointCloud<PointT> (it->voxels, str);
    	visualizer_->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_COLOR, r, g, b, str); 
		 
		if(visualize_centroid_and_normals)
		{
	    	//let's visualize also the centroids
	    	char str2[8];
	    	sprintf(str2,"sphere%d",counter);
	    	visualizer_->addSphere(it->centroid,0.005,1,0,0,str2);

	    	//Create the point clouds for the normals and centroids (actually the one of centroid is not useful but 
	    	// necessary to visualize the normals since their are defined as pcl::Normal)
	    	pcl::PointCloud<pcl::Normal>::Ptr nrm (new pcl::PointCloud<pcl::Normal>);
	    	nrm->points.push_back(it->normal);
	    	PointCloudT::Ptr ctr (new pcl::PointCloud<PointT>);
	    	ctr->points.push_back(it->centroid);
	    	char str3[8];
	    	sprintf(str3,"normal%d",counter);
	    	visualizer_->addPointCloudNormals<PointT,pcl::Normal>(ctr, nrm, 10, 0.05, str3);
    	}

    	counter++;    	
    }

    /*
    int counter = 0; //counter that helps to count how many facets have been encountered
    for (std::vector<std::vector<uint32_t> >::iterator it = facets_lbl.begin(); it != facets_lbl.end(); ++it)
    {
    	//get a random color for each new facet
    	float r, g, b, min, max;
    	r = static_cast <float> (rand()) / static_cast <float> (RAND_MAX);
    	g = static_cast <float> (rand()) / static_cast <float> (RAND_MAX);
    	b = static_cast <float> (rand()) / static_cast <float> (RAND_MAX);

    	Facet curr_facet = *it;

    	//Now let's color all the point of each supervoxel of the current facet
    	for (std::vector<uint32_t>::iterator i = curr_facet.begin(); i != curr_facet.end(); ++i)
    	{
    		uint32_t supervoxel_label = *i;
		    //Now get the supervoxel corresponding to the label
		    pcl::Supervoxel<PointT>::Ptr supervoxel = supervoxel_clusters.at (supervoxel_label);
		    PointTCloud voxels = supervoxel->voxels_;

			char str[30];
			sprintf(str,"line%d",counter);
	    	visualizer_->addPointCloud<PointT> (voxels, str);
    		visualizer_->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_COLOR, r, g, b, str); 
		 
    		counter++;
    	}
    	
    }*/

    //----------------create map of facet adjacency --------------------------
   
    pcl::console::print_highlight ("Creating adjacency facet map\n");
    //for (Facets::iterator i = facets_lbl.begin(); i != facets_lbl.end(); ++i) //pass all tthe facet
    for (uint32_t i = 0; i < facets_lbl.size(); ++i) //pass all the facets
    {
   		facets_adjacency.insert(std::pair<uint32_t, uint32_t>(i,i)); // each facets is connected to itself
    	for (Facet::iterator sv_it = (facets_lbl[i]).begin(); sv_it != (facets_lbl[i]).end(); ++sv_it)//pass all the supervoxel of the current facet
    	{
    		//for each sueprvoxel, we have to look if their adjacents belong to the same surface or not. 
    		//We get, with the following line, the connected labels
    		std::multimap<uint32_t,uint32_t>::iterator adjacent_itr = supervoxel_adjacency.equal_range ((*sv_it)).first;
		    for ( ; adjacent_itr!=supervoxel_adjacency.equal_range (*sv_it).second; ++adjacent_itr)
		    { 
		    	uint32_t ns_label = adjacent_itr->second;
		    	if(!sameFacet(ns_label,facets_lbl[i])) //if the voxel does not belong to the facet, we have to connect the 2 facets
		    	{	
		    		uint32_t adj_facet_label;
		    		bool term_condition = false;
		    		uint32_t h = 0;
		    		while( h < facets_lbl.size() && !term_condition) //look in the other surfaces for the current sueprvoxel
		    		{
		    			if(h != i) //we don't want to look in the same surface to understand what that belongs to 
		    			{
		    				std::vector<uint32_t>::iterator h2 = facets_lbl[h].begin();
		    				while ( h2 != facets_lbl[h].end() && !term_condition) // look in the current surface if there is the supervoxel
		    				{
		    					if( ns_label == (*h2) )
		    					{
		    						adj_facet_label = h;
		    						term_condition = true;
		    					}	
		    					++h2;
		    				}
		    			}
		    			++h;
		    		}
		    		facets_adjacency.insert(std::pair<uint32_t, uint32_t>(i,adj_facet_label));
		    	}
		    }
	 	}
	}

	if(visualize_adjacency_facets_map)
	{
       	displayFacetAdjacencyMap(facets_adjacency, facets, visualizer_);
	
    	while (!visualizer_->wasStopped ())
   		{
     		visualizer_->spinOnce (100);
   		}
    }

	return;
}


void clusteringByConcaveness(	std::multimap<uint32_t, uint32_t> supervoxel_adjacency,
								std::map <uint32_t, pcl::Supervoxel<PointT>::Ptr > supervoxel_clusters)
{



	return;
}