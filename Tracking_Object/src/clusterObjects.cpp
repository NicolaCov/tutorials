#include "clusterObjects.h"




bool visitedLabel_co(std::vector<uint32_t> visited, uint32_t label)
{
	for (std::vector<uint32_t>::iterator it = visited.begin(); it != visited.end(); it++)
		if( *it == label)
			return true;

	return false;
}

bool convexity(uint32_t l1, uint32_t l2, std::vector<Facet_supervoxel> &facets)
{

	Facet_supervoxel* f1 = &facets[l1];
	Facet_supervoxel* f2 = &facets[l2];

	//we create the vector as difference between f1.centroid and f2.centroid
	pcl::PointXYZRGBA c1 = f1->centroid;
	pcl::PointXYZRGBA c2 = f2->centroid;
	Eigen::Vector4f vec_centroid;
	vec_centroid << (c2.x - c1.x) , (c2.y - c1.y) , (c2.z - c1.z) , 0; 
	vec_centroid.normalize();

	if(vec_centroid.dot((f2->normal).getNormalVector4fMap()) < -0.05) // if their dot product is less than zero then they are concave
    //if(((f2->normal).getNormalVector4fMap()).dot((f1->normal).getNormalVector4fMap()) > 0) //this is another condition that actually is useless, I tried to add it in order to resolve the aprticualr case of bad segmentation
		  return false;

	return true;
}


bool convexity2(uint32_t l1, uint32_t l2, std::vector<Facet_supervoxel> &facets)
{
  Facet_supervoxel* f1 = &facets[l1];
  Facet_supervoxel* f2 = &facets[l2];

  pcl::KdTreeFLANN<PointT> kdtree; //create the kdtree structure

  kdtree.setInputCloud (f2->voxels);
  
  //seraching the nearest point
  PointT searchPoint = f1->centroid;
  std::vector<int> pointIdxNKNSearch(1);
  std::vector<float> pointNKNSquaredDistance(1);
  kdtree.nearestKSearch (searchPoint, 1, pointIdxNKNSearch, pointNKNSquaredDistance);
  PointT np = (f2->voxels->at(pointIdxNKNSearch[0]));

  pcl::PointXYZRGBA c1 = f1->centroid;
  Eigen::Vector4f vec_centroid;
  vec_centroid << (np.x - c1.x) , (np.y - c1.y) , (np.z - c1.z) , 0; 
  vec_centroid.normalize();

  if(vec_centroid.dot((f2->normal).getNormalVector4fMap()) < -0.1) // if their dot product is less than zero then they are concave
      return false;

  return true;
}

void mergeFacetIT(	Object & obj, Objects & objs, std::multimap<uint32_t, uint32_t> & facets_adjacency,
					std::vector<uint32_t> & visited,std::multimap<uint32_t,uint32_t> & objects_map,
					std::vector<Facet_supervoxel> &facets, uint32_t label)
{
	//Visit all the adjacent facets to the facet labeled with "label"
	std::multimap<uint32_t,uint32_t>::iterator adjacent_itr = facets_adjacency.equal_range (label).first;
  for ( ; adjacent_itr!=facets_adjacency.equal_range (label).second; ++adjacent_itr)
  {
   	uint32_t fc_label = adjacent_itr->second;
   	if(!visitedLabel_co(visited,fc_label)) //if it has not been visited
   		if(convexity(label,fc_label,facets))
   		{
   			//we now add the points/voxels of the new facet
   			*(obj.points) +=  *(facets[fc_label].voxels);
	  	   objects_map.insert(std::pair<uint32_t, uint32_t>((uint32_t)objs.size(),fc_label));
    		 visited.push_back(fc_label);

    		mergeFacetIT(obj,objs,facets_adjacency,visited,objects_map,facets,fc_label);

    	}

  }

	return;
}

void visualizeObjects(Objects &objs)
//void visualizeObjects(std::vector<Facet_supervoxel>  & objs)
{
	boost::shared_ptr<pcl::visualization::PCLVisualizer> visualizer_ (new pcl::visualization::PCLVisualizer ("Objects Segmentation"));
  visualizer_->setBackgroundColor (0, 0, 0);
  visualizer_->addCoordinateSystem (0.5);

  int counter = 0;
  for (Objects::iterator it = objs.begin(); it != objs.end(); ++it)
  //for (std::vector<Facet_supervoxel>::iterator it = objs.begin(); it != objs.end(); ++it)
  {	
   	//get a random color for each new object
   	float r, g, b, min, max;
   	r = static_cast <float> (rand()) / static_cast <float> (RAND_MAX);
   	g = static_cast <float> (rand()) / static_cast <float> (RAND_MAX);
   	b = static_cast <float> (rand()) / static_cast <float> (RAND_MAX);

   	char str[30];
  	sprintf(str,"object_%d",counter);
  	
    //visualizer_->addPointCloud<PointT> (it->voxels, str);
  	visualizer_->addPointCloud<PointT> (it->points, str);
    visualizer_->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_COLOR, r, g, b, str); 

    counter++;
  }
  while (!visualizer_->wasStopped ())
  {
   	visualizer_->spinOnce (100);
  }

	return;
}

void clusterObject(	std::multimap<uint32_t, uint32_t> & facets_adjacency, 
					std::vector<Facet_supervoxel> &facets, Facets &facets_lbl,
					Objects & objs)
{
	//for each possible facet
	std::multimap<uint32_t,uint32_t> objects_map;
  std::multimap<uint32_t,uint32_t>::iterator label_itr = facets_adjacency.begin ();
  std::vector<uint32_t> visited; //vector of labels to track what facets have been already added to an object
  for ( ; label_itr != facets_adjacency.end (); )
  {

   	uint32_t fc_label = label_itr->first;
	  if(!visitedLabel_co(visited,fc_label)) //it is a new object
	  {
  		visited.push_back(fc_label);

  		//now we get the facet corresponding to the label
  		Facet_supervoxel curr_facet = facets.at(fc_label);

  		objects_map.insert(std::pair<uint32_t, uint32_t>((uint32_t)objs.size(),fc_label));
  		Object obj;
      obj.points = curr_facet.voxels;
  		mergeFacetIT(obj,objs,facets_adjacency,visited,objects_map,facets,fc_label);
      
      if(obj.points->size() > 30 ) //It does not consider really small objects
        objs.push_back(obj);

	  }

     	label_itr = facets_adjacency.upper_bound (fc_label);
  }
  std::cout << "Number of objects: " << objs.size() << std::endl;
  //Now we have clustered the objects



	return;
}