#include "super_voxels.h"

 
 void
 addSupervoxelConnectionsToViewer (PointT &supervoxel_center,
                                   PointCloudT &adjacent_supervoxel_centers,
                                   std::string supervoxel_name,
                                   boost::shared_ptr<pcl::visualization::PCLVisualizer> & viewer)
{
   vtkSmartPointer<vtkPoints> points = vtkSmartPointer<vtkPoints>::New ();
   vtkSmartPointer<vtkCellArray> cells = vtkSmartPointer<vtkCellArray>::New ();
   vtkSmartPointer<vtkPolyLine> polyLine = vtkSmartPointer<vtkPolyLine>::New ();
 
   //Iterate through all adjacent points, and add a center point to adjacent point pair
   PointCloudT::iterator adjacent_itr = adjacent_supervoxel_centers.begin ();
   for ( ; adjacent_itr != adjacent_supervoxel_centers.end (); ++adjacent_itr)
   {
     points->InsertNextPoint (supervoxel_center.data);
     points->InsertNextPoint (adjacent_itr->data);
   }
   // Create a polydata to store everything in
   vtkSmartPointer<vtkPolyData> polyData = vtkSmartPointer<vtkPolyData>::New ();
   // Add the points to the dataset
   polyData->SetPoints (points);
   polyLine->GetPointIds  ()->SetNumberOfIds(points->GetNumberOfPoints ());
   for(unsigned int i = 0; i < points->GetNumberOfPoints (); i++)
     polyLine->GetPointIds ()->SetId (i,i);
   cells->InsertNextCell (polyLine);
   // Add the lines to the dataset
   polyData->SetLines (cells);
   viewer->addModelFromPolyData (polyData,supervoxel_name);
}
 
void VCCS(  pcl::SupervoxelClustering<PointT> & super, PointCloudT::Ptr cloud,
            std::map <uint32_t, pcl::Supervoxel<PointT>::Ptr > & supervoxel_clusters,
            std::multimap<uint32_t, uint32_t> & supervoxel_adjacency, option_sv & opt )
 {

   //pcl::SupervoxelClustering<PointT> super (voxel_resolution, seed_resolution, use_transform);

   super.setInputCloud (cloud);
   super.setColorImportance (opt.color_importance);
   super.setSpatialImportance (opt.spatial_importance);
   super.setNormalImportance (opt.normal_importance);
 
 
   super.extract (supervoxel_clusters);
   //super.refineSupervoxels (10,supervoxel_clusters);

   super.getSupervoxelAdjacency (supervoxel_adjacency);

  if (opt.visualization)
  {
    
  
   boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer (new pcl::visualization::PCLVisualizer ("3D Viewer"));
   viewer->setBackgroundColor (0, 0, 0);
 
   PointCloudT::Ptr voxel_centroid_cloud = super.getVoxelCentroidCloud ();
   viewer->addPointCloud (voxel_centroid_cloud, "voxel centroids");
   viewer->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE,2.0, "voxel centroids");
   viewer->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_OPACITY,0.95, "voxel centroids");
 
   PointCloudT::Ptr colored_voxel_cloud = super.getColoredVoxelCloud ();
   viewer->addPointCloud (colored_voxel_cloud, "colored voxels");
   viewer->addCoordinateSystem (0.5);
   viewer->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_OPACITY,0.8, "colored voxels");

   PointNCloudT::Ptr sv_normal_cloud = super.makeSupervoxelNormalCloud (supervoxel_clusters); 
   if(opt.view_normals)
   {
      //We have this disabled so graph is easy to see, uncomment to see supervoxel normals
      viewer->addPointCloudNormals<pcl::PointNormal> (sv_normal_cloud,1,0.05f, "supervoxel_normals");
   }
   
   //To make a graph of the supervoxel adjacency, we need to iterate through the supervoxel adjacency multimap
   std::multimap<uint32_t,uint32_t>::iterator label_itr = supervoxel_adjacency.begin ();
   for ( ; label_itr != supervoxel_adjacency.end (); )
   {
     //First get the label
     uint32_t supervoxel_label = label_itr->first;
     //Now get the supervoxel corresponding to the label
     pcl::Supervoxel<PointT>::Ptr supervoxel = supervoxel_clusters.at (supervoxel_label);
 
     //Now we need to iterate through the adjacent supervoxels and make a point cloud of them
     PointCloudT adjacent_supervoxel_centers;
     std::multimap<uint32_t,uint32_t>::iterator adjacent_itr = supervoxel_adjacency.equal_range (supervoxel_label).first;
     for ( ; adjacent_itr!=supervoxel_adjacency.equal_range (supervoxel_label).second; ++adjacent_itr)
     {
       pcl::Supervoxel<PointT>::Ptr neighbor_supervoxel = supervoxel_clusters.at (adjacent_itr->second);
       adjacent_supervoxel_centers.push_back (neighbor_supervoxel->centroid_);
     }
     //Now we make a name for this polygon
     std::stringstream ss;
     ss << "supervoxel_" << supervoxel_label;
     //This function is shown below, but is beyond the scope of this tutorial - basically it just generates a "star" polygon mesh from the points given
     addSupervoxelConnectionsToViewer (supervoxel->centroid_, adjacent_supervoxel_centers, ss.str (), viewer);
     //Move iterator forward to next label
     label_itr = supervoxel_adjacency.upper_bound (supervoxel_label);

   }

 
   while (!viewer->wasStopped ())
   {
     viewer->spinOnce (100);
   }
 }

 return;
}