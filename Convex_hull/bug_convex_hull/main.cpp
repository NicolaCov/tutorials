#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/io/pcd_io.h>
#include <pcl/surface/convex_hull.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <iostream>
#include <vector>

typedef pcl::PointXYZRGBNormal PointT;

boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer;
boost::shared_ptr<std::vector<pcl::Vertices> > vertices;
boost::shared_ptr<pcl::PointCloud<PointT> > cloud_hull;
boost::shared_ptr<pcl::PointCloud<PointT> > input;


void keyboard_callback (const pcl::visualization::KeyboardEvent& event, 
        void* cookie)
{        
    pcl::visualization::PointCloudColorHandlerCustom<PointT> color_handler1 (input, 255, 0, 0);    
    pcl::visualization::PointCloudColorHandlerCustom<PointT> color_handler2 (cloud_hull, 255, 0, 0);    

    if (event.keyUp())
    {        
        switch(event.getKeyCode())       
        {
            case '1': 
                viewer->removeAllPointClouds();
                viewer->removeAllShapes();
                viewer->addPointCloud(input, color_handler1, "input");
                break;    
            case '2': 
                viewer->removeAllPointClouds();
                viewer->removeAllShapes();
                viewer->addPointCloud(cloud_hull, color_handler2, "hull");
                break;    
            case '3':
                viewer->removeAllPointClouds();
                viewer->removeAllShapes();
                viewer->addPolygonMesh<PointT>(cloud_hull, *vertices);                
                break;    

        }
    }        
}

int
main (int argc, char** argv) 
{

    std::cout << "press 1 to see the input cloud, 2 to see the hull, 3 to see the meshed hull" << std::endl;
    
    viewer =  boost::shared_ptr<pcl::visualization::PCLVisualizer>
                        (new pcl::visualization::PCLVisualizer("PCL Visualizer"));      
    
    viewer->registerKeyboardCallback(&keyboard_callback, 0);

    input = boost::shared_ptr<pcl::PointCloud<PointT> > (new pcl::PointCloud<PointT>);
    
    if(pcl::io::loadPCDFile<PointT>("data.pcd", *input) == -1)
    {
        std::cout<<"ERROR LOADING THE CLOUD" <<std::endl;
        exit(1);
    }
    pcl::visualization::PointCloudColorHandlerCustom<PointT> color_handler (input, 255, 0, 0);    
    viewer->addPointCloud<PointT>(input, color_handler, "input");
    
    viewer->resetCameraViewpoint("input");
    
    cloud_hull = boost::shared_ptr<pcl::PointCloud<PointT> > (new pcl::PointCloud<PointT>);

    pcl::ConvexHull<PointT> chull;

    vertices = boost::shared_ptr<std::vector<pcl::Vertices> >(new std::vector<pcl::Vertices>); // to be meshed

    chull.setInputCloud (input);
    chull.setDimension(3);
    chull.setComputeAreaVolume(true);
    chull.reconstruct (*cloud_hull, *vertices);

    while (!viewer->wasStopped ())   
        viewer->spinOnce (100);
    
    return 0;

}
