#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <iostream>

// In opencv 3 CV_LOAD_IMAGE_COLOR is not define
#ifndef CV_LOAD_IMAGE_COLOR
#define CV_LOAD_IMAGE_COLOR 1
#endif

//#include "saliencySpecializedClasses.hpp"
#include <opencv2/saliency.hpp>
#include <opencv2/saliency/saliencySpecializedClasses.hpp>
#include <opencv2/core/utility.hpp>
#include <opencv2/highgui.hpp>
#include <iostream>

using namespace cv;
using namespace std;
using namespace saliency;

static const char* keys =
{ "{@saliency_algorithm | | Saliency algorithm <saliencyAlgorithmType.[saliencyAlgorithmTypeSubType]> }"
    "{@video_name      | | video name            }"
    "{@start_frame     |1| Start frame           }"
    "{@training_path   |1| Path of the folder containing the trained files}" };

int main( int argc, char** argv )
{
    if( argc != 2)
    {
     cout <<" Usage: display_image ImageToLoadAndDisplay" << endl;
     return -1;
    }

    cv::Mat image;
    image = cv::imread(argv[1], CV_LOAD_IMAGE_COLOR);   // Read the file

    if(! image.data )                              // Check for invalid input
    {
        cout <<  "Could not open or find the image" << std::endl ;
        return -1;
    }

    cv::namedWindow( "Display window", WINDOW_AUTOSIZE );// Create a window for display.
    cv::imshow( "Display window", image );                   // Show our image inside it.

    cv::waitKey(0);                                          // Wait for a keystroke in the window

   
    
    //CommandLineParser parser( argc, argv, keys );
    //String saliency_algorithm = parser.get<String>( 0 );


    //String training_path = parser.get<String>( 3 );
    String saliency_algorithm = "BING";
    String training_path = "../ObjectnessTrainedModel";
    vector<Vec4i> saliencyMap;

    Ptr<Saliency> saliencyAlgorithm = Saliency::create( saliency_algorithm );
    saliencyAlgorithm.dynamicCast<cv::saliency::ObjectnessBING>()->setTrainingPath( training_path );
    //saliencyAlgorithm.dynamicCast<cv::saliency::ObjectnessBING>()->setBBResDir( training_path + "/Results" );

    if( saliencyAlgorithm->computeSaliency( image, saliencyMap ) )
    {
        std::cout << "Objectness done" << std::endl;
    }

    std::vector<float> values = saliencyAlgorithm.dynamicCast<cv::saliency::ObjectnessBING>()->getobjectnessValues();

/*    if(values.size() == saliencyMap.size())
        std::cout << "same sizeee\n";

    std::cout << "size saliencyMap: " << saliencyMap.size() << std::endl;
/*
    //find the maximum score 
    float max = 0;
    int max_idx;
    for (int i = 0; i < values.size(); ++i)
    {
        if(values[i] > max)
        {
            max = values[i];
            max_idx = i;
        }
    }*/


    for (int i = 0; i < saliencyMap.size(); ++i)
    {
        cv::rectangle(image, Point (saliencyMap[i][0],saliencyMap[i][1]), Point(saliencyMap[i][2],saliencyMap[i][3]) , Scalar(255,0,0),1,8, 0);
    }

    cv::namedWindow( "Saliency Map", WINDOW_AUTOSIZE );// Create a window for display.
    cv::imshow( "Saliency Map", image );                   // Show our image inside it.

    cv::waitKey(0);


    //This functions writes in a "bb.txt" file the number of rectangles (1st rows) and the coordinates of the vertices of them
    saliencyAlgorithm.dynamicCast<cv::saliency::ObjectnessBING>()->write();


    //other algorithm
    //"BinWangApr2014"
    /*Ptr<Saliency> saliencyAlgorithm = Saliency::create( "BinWangApr2014" );


        saliencyAlgorithm.dynamicCast<MotionSaliencyBinWangApr2014>()->setImagesize( image.cols, image.rows );
        saliencyAlgorithm.dynamicCast<MotionSaliencyBinWangApr2014>()->init();
        cvtColor( image, image, COLOR_BGR2GRAY );

        Mat saliencyMap;
        if( saliencyAlgorithm->computeSaliency( image, saliencyMap ) )
        {
          std::cout << "current frame motion saliency done" << std::endl;
        }

        imshow( "image", image );
        imshow( "saliencyMap", saliencyMap * 255 );

        cv::waitKey(0);
    */
    return 0;
}