# CMAKE generated file: DO NOT EDIT!
# Generated by "Unix Makefiles" Generator, CMake Version 2.8

#=============================================================================
# Special targets provided by cmake.

# Disable implicit rules so canonical targets will work.
.SUFFIXES:

# Remove some rules from gmake that .SUFFIXES does not remove.
SUFFIXES =

.SUFFIXES: .hpux_make_needs_suffix_list

# Suppress display of executed commands.
$(VERBOSE).SILENT:

# A target that is always out of date.
cmake_force:
.PHONY : cmake_force

#=============================================================================
# Set environment variables for the build.

# The shell in which to execute make rules.
SHELL = /bin/sh

# The CMake executable.
CMAKE_COMMAND = /usr/bin/cmake

# The command to remove a file.
RM = /usr/bin/cmake -E remove -f

# Escaping for special characters.
EQUALS = =

# The program to use to edit the cache.
CMAKE_EDIT_COMMAND = /usr/bin/ccmake

# The top-level source directory on which CMake was run.
CMAKE_SOURCE_DIR = /home/ncovallero/Documents/Tutorials/ObjectNess

# The top-level build directory on which CMake was run.
CMAKE_BINARY_DIR = /home/ncovallero/Documents/Tutorials/ObjectNess/build

# Include any dependencies generated for this target.
include CMakeFiles/ObjectNess.dir/depend.make

# Include the progress variables for this target.
include CMakeFiles/ObjectNess.dir/progress.make

# Include the compile flags for this target's objects.
include CMakeFiles/ObjectNess.dir/flags.make

CMakeFiles/ObjectNess.dir/src/ObjectNess.cpp.o: CMakeFiles/ObjectNess.dir/flags.make
CMakeFiles/ObjectNess.dir/src/ObjectNess.cpp.o: ../src/ObjectNess.cpp
	$(CMAKE_COMMAND) -E cmake_progress_report /home/ncovallero/Documents/Tutorials/ObjectNess/build/CMakeFiles $(CMAKE_PROGRESS_1)
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Building CXX object CMakeFiles/ObjectNess.dir/src/ObjectNess.cpp.o"
	/usr/bin/c++   $(CXX_DEFINES) $(CXX_FLAGS) -o CMakeFiles/ObjectNess.dir/src/ObjectNess.cpp.o -c /home/ncovallero/Documents/Tutorials/ObjectNess/src/ObjectNess.cpp

CMakeFiles/ObjectNess.dir/src/ObjectNess.cpp.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CXX source to CMakeFiles/ObjectNess.dir/src/ObjectNess.cpp.i"
	/usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -E /home/ncovallero/Documents/Tutorials/ObjectNess/src/ObjectNess.cpp > CMakeFiles/ObjectNess.dir/src/ObjectNess.cpp.i

CMakeFiles/ObjectNess.dir/src/ObjectNess.cpp.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CXX source to assembly CMakeFiles/ObjectNess.dir/src/ObjectNess.cpp.s"
	/usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -S /home/ncovallero/Documents/Tutorials/ObjectNess/src/ObjectNess.cpp -o CMakeFiles/ObjectNess.dir/src/ObjectNess.cpp.s

CMakeFiles/ObjectNess.dir/src/ObjectNess.cpp.o.requires:
.PHONY : CMakeFiles/ObjectNess.dir/src/ObjectNess.cpp.o.requires

CMakeFiles/ObjectNess.dir/src/ObjectNess.cpp.o.provides: CMakeFiles/ObjectNess.dir/src/ObjectNess.cpp.o.requires
	$(MAKE) -f CMakeFiles/ObjectNess.dir/build.make CMakeFiles/ObjectNess.dir/src/ObjectNess.cpp.o.provides.build
.PHONY : CMakeFiles/ObjectNess.dir/src/ObjectNess.cpp.o.provides

CMakeFiles/ObjectNess.dir/src/ObjectNess.cpp.o.provides.build: CMakeFiles/ObjectNess.dir/src/ObjectNess.cpp.o

# Object files for target ObjectNess
ObjectNess_OBJECTS = \
"CMakeFiles/ObjectNess.dir/src/ObjectNess.cpp.o"

# External object files for target ObjectNess
ObjectNess_EXTERNAL_OBJECTS =

../bin/ObjectNess: CMakeFiles/ObjectNess.dir/src/ObjectNess.cpp.o
../bin/ObjectNess: CMakeFiles/ObjectNess.dir/build.make
../bin/ObjectNess: /usr/lib/x86_64-linux-gnu/libboost_system.so
../bin/ObjectNess: /usr/lib/x86_64-linux-gnu/libboost_filesystem.so
../bin/ObjectNess: /usr/lib/x86_64-linux-gnu/libboost_thread.so
../bin/ObjectNess: /usr/lib/x86_64-linux-gnu/libboost_date_time.so
../bin/ObjectNess: /usr/lib/x86_64-linux-gnu/libboost_iostreams.so
../bin/ObjectNess: /usr/lib/x86_64-linux-gnu/libboost_serialization.so
../bin/ObjectNess: /usr/lib/x86_64-linux-gnu/libpthread.so
../bin/ObjectNess: /usr/local/lib/libpcl_common.so
../bin/ObjectNess: /usr/lib/x86_64-linux-gnu/libflann_cpp_s.a
../bin/ObjectNess: /usr/local/lib/libpcl_kdtree.so
../bin/ObjectNess: /usr/local/lib/libpcl_octree.so
../bin/ObjectNess: /usr/local/lib/libpcl_search.so
../bin/ObjectNess: /usr/local/lib/libpcl_sample_consensus.so
../bin/ObjectNess: /usr/local/lib/libpcl_filters.so
../bin/ObjectNess: /usr/lib/libOpenNI.so
../bin/ObjectNess: /usr/lib/libOpenNI2.so
../bin/ObjectNess: /usr/lib/libvtkCommon.so.5.8.0
../bin/ObjectNess: /usr/lib/libvtkFiltering.so.5.8.0
../bin/ObjectNess: /usr/lib/libvtkImaging.so.5.8.0
../bin/ObjectNess: /usr/lib/libvtkGraphics.so.5.8.0
../bin/ObjectNess: /usr/lib/libvtkGenericFiltering.so.5.8.0
../bin/ObjectNess: /usr/lib/libvtkIO.so.5.8.0
../bin/ObjectNess: /usr/lib/libvtkRendering.so.5.8.0
../bin/ObjectNess: /usr/lib/libvtkVolumeRendering.so.5.8.0
../bin/ObjectNess: /usr/lib/libvtkHybrid.so.5.8.0
../bin/ObjectNess: /usr/lib/libvtkWidgets.so.5.8.0
../bin/ObjectNess: /usr/lib/libvtkParallel.so.5.8.0
../bin/ObjectNess: /usr/lib/libvtkInfovis.so.5.8.0
../bin/ObjectNess: /usr/lib/libvtkGeovis.so.5.8.0
../bin/ObjectNess: /usr/lib/libvtkViews.so.5.8.0
../bin/ObjectNess: /usr/lib/libvtkCharts.so.5.8.0
../bin/ObjectNess: /usr/local/lib/libpcl_io.so
../bin/ObjectNess: /usr/local/lib/libpcl_features.so
../bin/ObjectNess: /usr/local/lib/libpcl_visualization.so
../bin/ObjectNess: /usr/local/lib/libpcl_ml.so
../bin/ObjectNess: /usr/local/lib/libpcl_segmentation.so
../bin/ObjectNess: /usr/local/lib/libpcl_people.so
../bin/ObjectNess: /usr/local/lib/libpcl_registration.so
../bin/ObjectNess: /usr/local/lib/libpcl_keypoints.so
../bin/ObjectNess: /usr/local/lib/libpcl_outofcore.so
../bin/ObjectNess: /usr/local/lib/libpcl_stereo.so
../bin/ObjectNess: /usr/lib/x86_64-linux-gnu/libqhull.so
../bin/ObjectNess: /usr/local/lib/libpcl_surface.so
../bin/ObjectNess: /usr/local/lib/libpcl_recognition.so
../bin/ObjectNess: /usr/local/lib/libpcl_tracking.so
../bin/ObjectNess: /usr/lib/x86_64-linux-gnu/libboost_system.so
../bin/ObjectNess: /usr/lib/x86_64-linux-gnu/libboost_filesystem.so
../bin/ObjectNess: /usr/lib/x86_64-linux-gnu/libboost_thread.so
../bin/ObjectNess: /usr/lib/x86_64-linux-gnu/libboost_date_time.so
../bin/ObjectNess: /usr/lib/x86_64-linux-gnu/libboost_iostreams.so
../bin/ObjectNess: /usr/lib/x86_64-linux-gnu/libboost_serialization.so
../bin/ObjectNess: /usr/lib/x86_64-linux-gnu/libpthread.so
../bin/ObjectNess: /usr/lib/x86_64-linux-gnu/libqhull.so
../bin/ObjectNess: /usr/lib/libOpenNI.so
../bin/ObjectNess: /usr/lib/libOpenNI2.so
../bin/ObjectNess: /usr/lib/x86_64-linux-gnu/libflann_cpp_s.a
../bin/ObjectNess: /usr/lib/libvtkCommon.so.5.8.0
../bin/ObjectNess: /usr/lib/libvtkFiltering.so.5.8.0
../bin/ObjectNess: /usr/lib/libvtkImaging.so.5.8.0
../bin/ObjectNess: /usr/lib/libvtkGraphics.so.5.8.0
../bin/ObjectNess: /usr/lib/libvtkGenericFiltering.so.5.8.0
../bin/ObjectNess: /usr/lib/libvtkIO.so.5.8.0
../bin/ObjectNess: /usr/lib/libvtkRendering.so.5.8.0
../bin/ObjectNess: /usr/lib/libvtkVolumeRendering.so.5.8.0
../bin/ObjectNess: /usr/lib/libvtkHybrid.so.5.8.0
../bin/ObjectNess: /usr/lib/libvtkWidgets.so.5.8.0
../bin/ObjectNess: /usr/lib/libvtkParallel.so.5.8.0
../bin/ObjectNess: /usr/lib/libvtkInfovis.so.5.8.0
../bin/ObjectNess: /usr/lib/libvtkGeovis.so.5.8.0
../bin/ObjectNess: /usr/lib/libvtkViews.so.5.8.0
../bin/ObjectNess: /usr/lib/libvtkCharts.so.5.8.0
../bin/ObjectNess: /usr/lib/x86_64-linux-gnu/libopencv_videostab.so.2.4.8
../bin/ObjectNess: /usr/lib/x86_64-linux-gnu/libopencv_video.so.2.4.8
../bin/ObjectNess: /usr/lib/x86_64-linux-gnu/libopencv_ts.so.2.4.8
../bin/ObjectNess: /usr/lib/x86_64-linux-gnu/libopencv_superres.so.2.4.8
../bin/ObjectNess: /usr/lib/x86_64-linux-gnu/libopencv_stitching.so.2.4.8
../bin/ObjectNess: /usr/lib/x86_64-linux-gnu/libopencv_photo.so.2.4.8
../bin/ObjectNess: /usr/lib/x86_64-linux-gnu/libopencv_ocl.so.2.4.8
../bin/ObjectNess: /usr/lib/x86_64-linux-gnu/libopencv_objdetect.so.2.4.8
../bin/ObjectNess: /usr/lib/x86_64-linux-gnu/libopencv_ml.so.2.4.8
../bin/ObjectNess: /usr/lib/x86_64-linux-gnu/libopencv_legacy.so.2.4.8
../bin/ObjectNess: /usr/lib/x86_64-linux-gnu/libopencv_imgproc.so.2.4.8
../bin/ObjectNess: /usr/lib/x86_64-linux-gnu/libopencv_highgui.so.2.4.8
../bin/ObjectNess: /usr/lib/x86_64-linux-gnu/libopencv_gpu.so.2.4.8
../bin/ObjectNess: /usr/lib/x86_64-linux-gnu/libopencv_flann.so.2.4.8
../bin/ObjectNess: /usr/lib/x86_64-linux-gnu/libopencv_features2d.so.2.4.8
../bin/ObjectNess: /usr/lib/x86_64-linux-gnu/libopencv_core.so.2.4.8
../bin/ObjectNess: /usr/lib/x86_64-linux-gnu/libopencv_contrib.so.2.4.8
../bin/ObjectNess: /usr/lib/x86_64-linux-gnu/libopencv_calib3d.so.2.4.8
../bin/ObjectNess: /usr/local/lib/libpcl_common.so
../bin/ObjectNess: /usr/local/lib/libpcl_kdtree.so
../bin/ObjectNess: /usr/local/lib/libpcl_octree.so
../bin/ObjectNess: /usr/local/lib/libpcl_search.so
../bin/ObjectNess: /usr/local/lib/libpcl_sample_consensus.so
../bin/ObjectNess: /usr/local/lib/libpcl_filters.so
../bin/ObjectNess: /usr/local/lib/libpcl_io.so
../bin/ObjectNess: /usr/local/lib/libpcl_features.so
../bin/ObjectNess: /usr/local/lib/libpcl_visualization.so
../bin/ObjectNess: /usr/local/lib/libpcl_ml.so
../bin/ObjectNess: /usr/local/lib/libpcl_segmentation.so
../bin/ObjectNess: /usr/local/lib/libpcl_people.so
../bin/ObjectNess: /usr/local/lib/libpcl_registration.so
../bin/ObjectNess: /usr/local/lib/libpcl_keypoints.so
../bin/ObjectNess: /usr/local/lib/libpcl_outofcore.so
../bin/ObjectNess: /usr/local/lib/libpcl_stereo.so
../bin/ObjectNess: /usr/local/lib/libpcl_surface.so
../bin/ObjectNess: /usr/local/lib/libpcl_recognition.so
../bin/ObjectNess: /usr/local/lib/libpcl_tracking.so
../bin/ObjectNess: /usr/lib/libvtkViews.so.5.8.0
../bin/ObjectNess: /usr/lib/libvtkInfovis.so.5.8.0
../bin/ObjectNess: /usr/lib/libvtkWidgets.so.5.8.0
../bin/ObjectNess: /usr/lib/libvtkVolumeRendering.so.5.8.0
../bin/ObjectNess: /usr/lib/libvtkHybrid.so.5.8.0
../bin/ObjectNess: /usr/lib/libvtkParallel.so.5.8.0
../bin/ObjectNess: /usr/lib/libvtkRendering.so.5.8.0
../bin/ObjectNess: /usr/lib/libvtkImaging.so.5.8.0
../bin/ObjectNess: /usr/lib/libvtkGraphics.so.5.8.0
../bin/ObjectNess: /usr/lib/libvtkIO.so.5.8.0
../bin/ObjectNess: /usr/lib/libvtkFiltering.so.5.8.0
../bin/ObjectNess: /usr/lib/libvtkCommon.so.5.8.0
../bin/ObjectNess: /usr/lib/libvtksys.so.5.8.0
../bin/ObjectNess: /usr/lib/x86_64-linux-gnu/libopencv_photo.so.2.4.8
../bin/ObjectNess: /usr/lib/x86_64-linux-gnu/libopencv_legacy.so.2.4.8
../bin/ObjectNess: /usr/lib/x86_64-linux-gnu/libopencv_video.so.2.4.8
../bin/ObjectNess: /usr/lib/x86_64-linux-gnu/libopencv_objdetect.so.2.4.8
../bin/ObjectNess: /usr/lib/x86_64-linux-gnu/libopencv_ml.so.2.4.8
../bin/ObjectNess: /usr/lib/x86_64-linux-gnu/libopencv_calib3d.so.2.4.8
../bin/ObjectNess: /usr/lib/x86_64-linux-gnu/libopencv_features2d.so.2.4.8
../bin/ObjectNess: /usr/lib/x86_64-linux-gnu/libopencv_highgui.so.2.4.8
../bin/ObjectNess: /usr/lib/x86_64-linux-gnu/libopencv_imgproc.so.2.4.8
../bin/ObjectNess: /usr/lib/x86_64-linux-gnu/libopencv_flann.so.2.4.8
../bin/ObjectNess: /usr/lib/x86_64-linux-gnu/libopencv_core.so.2.4.8
../bin/ObjectNess: CMakeFiles/ObjectNess.dir/link.txt
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --red --bold "Linking CXX executable ../bin/ObjectNess"
	$(CMAKE_COMMAND) -E cmake_link_script CMakeFiles/ObjectNess.dir/link.txt --verbose=$(VERBOSE)

# Rule to build all files generated by this target.
CMakeFiles/ObjectNess.dir/build: ../bin/ObjectNess
.PHONY : CMakeFiles/ObjectNess.dir/build

CMakeFiles/ObjectNess.dir/requires: CMakeFiles/ObjectNess.dir/src/ObjectNess.cpp.o.requires
.PHONY : CMakeFiles/ObjectNess.dir/requires

CMakeFiles/ObjectNess.dir/clean:
	$(CMAKE_COMMAND) -P CMakeFiles/ObjectNess.dir/cmake_clean.cmake
.PHONY : CMakeFiles/ObjectNess.dir/clean

CMakeFiles/ObjectNess.dir/depend:
	cd /home/ncovallero/Documents/Tutorials/ObjectNess/build && $(CMAKE_COMMAND) -E cmake_depends "Unix Makefiles" /home/ncovallero/Documents/Tutorials/ObjectNess /home/ncovallero/Documents/Tutorials/ObjectNess /home/ncovallero/Documents/Tutorials/ObjectNess/build /home/ncovallero/Documents/Tutorials/ObjectNess/build /home/ncovallero/Documents/Tutorials/ObjectNess/build/CMakeFiles/ObjectNess.dir/DependInfo.cmake --color=$(COLOR)
.PHONY : CMakeFiles/ObjectNess.dir/depend

