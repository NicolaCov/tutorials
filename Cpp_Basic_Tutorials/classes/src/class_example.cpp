#include <iostream>
using namespace std;

class CVector {
  public:
    int x,y;
    CVector () {}
    CVector (int a, int b) : x(a), y(b) {}

   CVector& operator= (const CVector& param)
   {
      this->x=param.x;
      this->y=param.y;
      return *this;
   }
   
};

CVector operator+ (const CVector& lhs, const CVector& rhs)
{
  CVector temp;
  temp.x = lhs.x + rhs.x;
  temp.y = lhs.y + rhs.y;
  return temp;
}

int main () {
  CVector foo (3,1);
  CVector bar (1,2);
  CVector result;
  result = foo + bar;
  cout << result.x << ',' << result.y << '\n';

  CVector bar2;
  bar2 = bar;
  cout << bar.x << ',' << bar.y << "  " << bar2.x << ',' << bar2.y << '\n';
  bar2.operator= (foo);
  cout << foo.x << ',' << foo.y << "  " << bar2.x << ',' << bar2.y << '\n';

  return 0;
}