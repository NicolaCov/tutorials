// a usefuk tutorial of eigen vector/matrix is here:
// http://eigen.tuxfamily.org/dox-2.0/TutorialCore.html

#include <pcl/io/pcd_io.h>
#include <iostream>
#include <Eigen/Dense>

int main (int argc, char ** argv)
{

   	Eigen::Vector4f p ,q;
   	p << 1 , 2 , 1 , 0;
   	p.normalize(); // normalize the vector p
   	//q = p;
   	q << 1 , 2 , 0.99, 0;
   	q.normalize();

	float r = p.dot (q);
	float r2 = q.dot(p);

	std::cout << "The result of the dot product is: " << r << "   q.p = "<< r2 <<std::endl;

	
	return 0;
}