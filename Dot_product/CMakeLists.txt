cmake_minimum_required(VERSION 2.8 FATAL_ERROR)

project(dot_product_example)

find_package(PCL 1.7 REQUIRED)

include_directories(./include ${PCL_INCLUDE_DIRS})
link_directories(${PCL_LIBRARY_DIRS})
add_definitions(${PCL_DEFINITIONS})

add_executable (dot_product_example src/dot_product_example.cpp)
target_link_libraries (dot_product_example ${PCL_LIBRARIES})
set_target_properties(dot_product_example PROPERTIES RUNTIME_OUTPUT_DIRECTORY ${PROJECT_SOURCE_DIR}/bin)