#include <iostream>
#include <pcl/ModelCoefficients.h>
#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>
#include <pcl/sample_consensus/method_types.h>
#include <pcl/sample_consensus/model_types.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/common/common.h>
#include <pcl/surface/convex_hull.h>
#include <pcl/segmentation/extract_polygonal_prism_data.h>
#include <pcl/range_image/range_image.h>
#include <pcl/io/pcd_io.h>
#include <pcl/visualization/range_image_visualizer.h>

bool next = false; //to go to the next step

// Types
typedef pcl::PointXYZRGBA PointT;

void help()
{
   std::cout << "argv[1] = pcd to work with\n";
   std::cout << "argv[2] = distance treshold for RANSAC plane estimation\n";
   return;
}

boost::shared_ptr<pcl::RangeImage> getRangeImage(pcl::PointCloud<pcl::PointXYZRGBA> & point_cloud)
{
  // --------------------
  // -----Parameters-----
  // --------------------
  float angular_resolution_x = 0.5f,
        angular_resolution_y = angular_resolution_x;
  pcl::RangeImage::CoordinateFrame coordinate_frame = pcl::RangeImage::CAMERA_FRAME;
  bool live_update = false;

  Eigen::Affine3f scene_sensor_pose (Eigen::Affine3f::Identity ());
  scene_sensor_pose = Eigen::Affine3f (Eigen::Translation3f (point_cloud.sensor_origin_[0],
                                                             point_cloud.sensor_origin_[1],
                                                             point_cloud.sensor_origin_[2])) *
                        Eigen::Affine3f (point_cloud.sensor_orientation_);

  // -----------------------------------------------
  // -----Create RangeImage from the PointCloud-----
  // -----------------------------------------------
  float noise_level = 0.0;
  float min_range = 0.0f;
  int border_size = 0.0f;
  boost::shared_ptr<pcl::RangeImage> range_image_ptr(new pcl::RangeImage);
  pcl::RangeImage& range_image = *range_image_ptr;   
  range_image.createFromPointCloud (point_cloud, angular_resolution_x, angular_resolution_y,
                                    pcl::deg2rad (360.0f), pcl::deg2rad (180.0f),
                                    scene_sensor_pose, coordinate_frame, noise_level, min_range, border_size);
  return range_image_ptr;
}

void keyboardEventOccurred (const pcl::visualization::KeyboardEvent &event,
                            void* viewer_void)
{
  boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer = *static_cast<boost::shared_ptr<pcl::visualization::PCLVisualizer> *> (viewer_void);
  if (event.getKeySym () == "n" && event.keyDown ())
  {
    std::cout << "n was pressed => next step" << std::endl;
    viewer->removePointCloud("Original");
    next = true;
  }
  else if(event.keyDown ())
    std::cout << "the only valid key is n\n";
}

int
 main (int argc, char** argv)
{

  double th_ransac;
  if (argc < 2)
  {
	help();
  	return 0;
  }
  else if(argc  < 3)
     th_ransac = 0.02;
  else
     th_ransac = atof(argv[2]);
  

  pcl::PointCloud<PointT>::Ptr cloud(new pcl::PointCloud<PointT>);
  pcl::PointCloud<PointT>::Ptr original_cloud(new pcl::PointCloud<PointT>);

  pcl::console::print_highlight ("Loading point cloud...\n");
  if (pcl::io::loadPCDFile<PointT> (argv[1], *cloud))
  {
    pcl::console::print_error ("Error loading cloud file!\n");
    return (1);
  }
  std::cout << "pcd loaded  " << cloud->points.size() <<"points\n";

  //visualize original point cloud
  boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer (new pcl::visualization::PCLVisualizer ("Original Point cloud"));
  viewer->setBackgroundColor (255, 255, 255);
  pcl::visualization::PointCloudColorHandlerRGBField<pcl::PointXYZRGBA> rgb_cloud_obj(cloud);
  // viewer->addPointCloud<pcl::PointXYZRGBA> (cloud, rgb_cloud_obj, "Original");
  pcl::visualization::PointCloudColorHandlerGenericField<pcl::PointXYZRGBA> handler(cloud,"z");
  viewer->addPointCloud<pcl::PointXYZRGBA> (cloud, handler, "Original");
  


  // boost::shared_ptr<pcl::RangeImage> range_image_ptr = getRangeImage(*cloud);
  // pcl::visualization::PointCloudColorHandlerCustom<pcl::PointWithRange> range_image_color_handler (range_image_ptr, 0, 0, 0);
  // viewer->addPointCloud (range_image_ptr, range_image_color_handler, "range image");
  // viewer->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE,2.0, "range image");
  // viewer->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_OPACITY,0.95, "range image");

  viewer->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE,2.0, "Original");
  viewer->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_OPACITY,0.95, "Original");
  //viewer->addCoordinateSystem (0.1);//the number is the length of the axis

  viewer->initCameraParameters ();
  viewer->registerKeyboardCallback (keyboardEventOccurred, (void*)&viewer);
  while (!viewer->wasStopped() && !next)
  {
    viewer->spinOnce (100);
  }
  next = false;
  viewer->removePointCloud("Original");

  pcl::console::print_highlight ("Estimating plane...\n");
  pcl::ModelCoefficients::Ptr coefficients (new pcl::ModelCoefficients);
  pcl::PointIndices::Ptr inliers (new pcl::PointIndices);
  // Create the segmentation object
  pcl::SACSegmentation<PointT> seg;
  // Optional
  seg.setOptimizeCoefficients (true);
  // Mandatory
  seg.setModelType (pcl::SACMODEL_PLANE);
  seg.setMethodType (pcl::SAC_RANSAC);
  seg.setDistanceThreshold (th_ransac);

  seg.setInputCloud (cloud);
  seg.segment (*inliers, *coefficients);

  if (inliers->indices.size () == 0)
  {
    PCL_ERROR ("Could not estimate a planar model for the given dataset.");
    return (-1);
  }

  //set color of inliers
  for (size_t i = 0; i < inliers->indices.size (); ++i)
  {
	cloud->points[inliers->indices[i]].r = 0; 
	cloud->points[inliers->indices[i]].g = 0; 
	cloud->points[inliers->indices[i]].b = 0; 
  }

  //rgb_cloud_obj.setInputCloud(cloud);
  handler.setInputCloud(cloud);
  

  viewer->addPointCloud<pcl::PointXYZRGBA> (cloud, rgb_cloud_obj, "Original");
  //viewer->addPointCloud<pcl::PointXYZRGBA> (cloud, handler, "Original");
  viewer->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE,2.0, "Original");
  viewer->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_OPACITY,0.95, "Original");
  while (!viewer->wasStopped() && !next)
  {
    viewer->spinOnce (100);
  }
  next = false;
  viewer->removePointCloud("Original");


  viewer->addPointCloud<pcl::PointXYZRGBA> (cloud, handler, "Original");
  // Extract inliers from the input cloud
  pcl::console::print_highlight ("Obtaining plane...\n");
  pcl::ExtractIndices<PointT> extract ;
  extract.setInputCloud (cloud);
  extract.setIndices (inliers);
  extract.setNegative (false); // get all the points with index specified by indices from the point cloud "cloud"
  extract.filter (*cloud); 

  //visualize inliers
  rgb_cloud_obj.setInputCloud(cloud);
  viewer->addPointCloud<pcl::PointXYZRGBA> (cloud, rgb_cloud_obj, "table");
  // handler.setInputCloud(cloud);
  // viewer->addPointCloud<pcl::PointXYZRGBA> (cloud, handler, "Original");
  // viewer->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE,2.0, "Original");
  // viewer->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_OPACITY,0.95, "Original");
  while (!viewer->wasStopped () && !next)
  {
    viewer->spinOnce (100);
  }
  next = false;

  // convex hull 

  boost::shared_ptr<pcl::PointCloud<PointT> > cloud_hull = boost::shared_ptr<pcl::PointCloud<PointT> > (new pcl::PointCloud<PointT>);

  pcl::ConvexHull<PointT> chull;

  boost::shared_ptr<std::vector<pcl::Vertices> > vertices = boost::shared_ptr<std::vector<pcl::Vertices> >(new std::vector<pcl::Vertices>); // to be meshed


  pcl::console::print_highlight ("Convex Hull...\n");
  chull.setInputCloud (cloud);
  chull.setDimension(2);//here u can choose 2 (or 3) dimensions
  chull.setComputeAreaVolume(true);
  chull.reconstruct (*cloud_hull, *vertices);

  viewer->removeAllPointClouds();
  viewer->removeAllShapes();
  viewer->addPolygonMesh<PointT>(cloud_hull, *vertices);                

  while (!viewer->wasStopped () && !next)
  {
    viewer->spinOnce (100);
  }
  next = false;


  if (pcl::io::loadPCDFile<PointT> (argv[1], *original_cloud))
  {
    pcl::console::print_error ("Error loading cloud file!\n");
    return (1);
  }
  
  if (chull.getDimension() == 2)// Redundant check.
  {
    // Prism object.
    pcl::ExtractPolygonalPrismData<pcl::PointXYZRGBA> prism;
    prism.setInputCloud(original_cloud);
    prism.setInputPlanarHull(cloud_hull);
    // First parameter: minimum Z value. Set to 0, segments objects lying on the plane (can be negative).
    // Second parameter: maximum Z value, set to 10cm. Tune it according to the height of the objects you expect.
    prism.setHeightLimits(0.05,1.0);
    pcl::PointIndices::Ptr objectIndices(new pcl::PointIndices);

    prism.segment(*objectIndices);

    // Get and show all points retrieved by the hull.
    extract.setInputCloud (original_cloud);
    extract.setIndices(objectIndices);
    extract.filter(*original_cloud);
  }
  else std::cout << "The chosen convex hull is not planar." << std::endl;

  std::cout << "Showing also the detected object\n";

  // add point cloud of detected objects
  // rgb_cloud_obj.setInputCloud(original_cloud);
  // viewer->addPointCloud<pcl::PointXYZRGBA> (original_cloud, rgb_cloud_obj, "Objects");
  pcl::visualization::PointCloudColorHandlerGenericField<pcl::PointXYZRGBA> handler2(original_cloud,"z");
  viewer->addPointCloud<pcl::PointXYZRGBA> (original_cloud, handler2, "Objects");
  
  
  //viewer->addPointCloud<pcl::PointXYZRGBA> (original_cloud, handler, "Objects");
  viewer->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE,2.0, "Objects");
  viewer->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_OPACITY,0.95, "Objects");

  while (!viewer->wasStopped () && !next)
  {
    viewer->spinOnce (100);
  }
  next = false;

  std::cout << "Removing the convex Hull\n";
  viewer->removePolygonMesh();

  while (!viewer->wasStopped () && !next)
  {
    viewer->spinOnce (100);
  }
  
  //save in a pcd file
  pcl::io::savePCDFileASCII ("test_pcd.pcd", *original_cloud);
  std::cout << "Saved: test_pcd.pcd\n";

  return (0);
}
