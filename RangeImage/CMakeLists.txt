cmake_minimum_required(VERSION 2.8 FATAL_ERROR)

project(RangeImage)

find_package(PCL 1.8 REQUIRED)
find_package( OpenCV REQUIRED )

include_directories(./include ${PCL_INCLUDE_DIRS})
link_directories(${PCL_LIBRARY_DIRS})
add_definitions(${PCL_DEFINITIONS})

add_executable (RangeImage src/RangeImage.cpp)
target_link_libraries (RangeImage ${PCL_LIBRARIES} ${OpenCV_LIBS}) 
set_target_properties(RangeImage PROPERTIES RUNTIME_OUTPUT_DIRECTORY ${PROJECT_SOURCE_DIR}/bin)