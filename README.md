# **Tutorials** #

This repo contains some tutorials.

Usefull tutorial:

* RANSAC_plane_removal: estimation and removal of a plane

* Convex_hull: convex hull of a certain point cloud given as input and its visualziation

* Convex_hull_plane: convex hull of plane (3 or 2 dimensional) and its visualziation

* VCCS segmentation: 
in build/VCCS you can see simply the supervoxel
in bin/VCCS_removeTable: you can see the whole segmentation: facets and adiacency graph and the resultant object on the tabletop objects.

* LCCP_segmentation: bin/LCCP this segment the point cloud, detects the table and the table top objects (maybe doing before the table detection and then the segmentation would be better in time and results), then shows one by one the objects in the point cloud and in the rgb image