cmake_minimum_required(VERSION 2.8 FATAL_ERROR)

project(supervoxel_example)

find_package(PCL 1.7 REQUIRED)

include_directories( ${PCL_INCLUDE_DIRS})
link_directories(${PCL_LIBRARY_DIRS})
add_definitions(${PCL_DEFINITIONS})


add_executable (supervoxel_example src/supervoxel_example.cpp)
target_link_libraries (supervoxel_example ${PCL_LIBRARIES} ${OpenCV_LIBS})
set_target_properties (supervoxel_example PROPERTIES RUNTIME_OUTPUT_DIRECTORY ${PROJECT_SOURCE_DIR}/bin)