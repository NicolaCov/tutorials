#include <pcl/ModelCoefficients.h>
#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>
#include <pcl/sample_consensus/method_types.h>
#include <pcl/sample_consensus/model_types.h>
#include <pcl/filters/passthrough.h>
#include <pcl/filters/project_inliers.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/surface/convex_hull.h>
 #include <pcl/visualization/pcl_visualizer.h>
#include <pcl/filters/extract_indices.h>


#include <pcl/segmentation/extract_polygonal_prism_data.h>

int
main (int argc, char** argv)
{
  pcl::PointCloud<pcl::PointXYZ>::Ptr cloud (new pcl::PointCloud<pcl::PointXYZ>), 
                                      plane (new pcl::PointCloud<pcl::PointXYZ>), 
                                      cloud_filtered (new pcl::PointCloud<pcl::PointXYZ>), 
                                      cloud_projected (new pcl::PointCloud<pcl::PointXYZ>);

  pcl::console::print_highlight ("Loading point cloud...\n");
  if (pcl::io::loadPCDFile<pcl::PointXYZ> (argv[1], *cloud))
  {
    pcl::console::print_error ("Error loading cloud file!\n");
    return (1);
  }
  std::cout << "pcd loaded\n";  // Build a filter to remove spurious NaNs
  
  pcl::PassThrough<pcl::PointXYZ> pass;
  pass.setInputCloud (cloud);
  pass.setFilterFieldName ("z");
  pass.setFilterLimits (0, 1.1);
  pass.filter (*cloud_filtered);
  std::cerr << "PointCloud after filtering has: "
            << cloud_filtered->points.size () << " data points." << std::endl;

  boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer1 (new pcl::visualization::PCLVisualizer ("3D Viewer"));
   viewer1->setBackgroundColor (0, 0, 0);
 
   viewer1->addPointCloud (cloud, "voxel centroids");
   viewer1->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE,2.0, "voxel centroids");
   viewer1->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_OPACITY,0.95, "voxel centroids");

   while (!viewer1->wasStopped ())
   {
     viewer1->spinOnce (100);
   }


  pcl::ModelCoefficients::Ptr coefficients (new pcl::ModelCoefficients);
  pcl::PointIndices::Ptr inliers (new pcl::PointIndices);
  // Create the segmentation object
  pcl::SACSegmentation<pcl::PointXYZ> seg;
  // Optional
  seg.setOptimizeCoefficients (true);
  // Mandatory
  seg.setModelType (pcl::SACMODEL_PLANE);
  seg.setMethodType (pcl::SAC_RANSAC);
  seg.setDistanceThreshold (0.01);

  seg.setInputCloud (cloud_filtered);
  seg.segment (*inliers, *coefficients);
  std::cerr << "PointCloud after segmentation has: "
            << inliers->indices.size () << " inliers." << std::endl;

  pcl::ExtractIndices<pcl::PointXYZ> extract;
  extract.setInputCloud(cloud);
  extract.setIndices(inliers);
  extract.filter(*plane);


  // Project the model inliers
  pcl::ProjectInliers<pcl::PointXYZ> proj;
  proj.setModelType (pcl::SACMODEL_PLANE);
  proj.setIndices (inliers);
  proj.setInputCloud (cloud_filtered);
  proj.setModelCoefficients (coefficients);
  proj.filter (*cloud_projected);
  std::cerr << "PointCloud after projection has: "
            << cloud_projected->points.size () << " data points." << std::endl;

  // Create a convex Hull representation of the projected inliers
  pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_hull (new pcl::PointCloud<pcl::PointXYZ>);
  pcl::ConvexHull<pcl::PointXYZ> chull;
  chull.setInputCloud (cloud_projected);
  //chull.setAlpha (0.1);
  chull.reconstruct (*cloud_hull);

  std::cerr << "convex hull has: " << cloud_hull->points.size ()
            << " data points." << std::endl;

  //pcl::PCDWriter writer;
  //writer.write ("table_scene_mug_stereo_textured_hull.pcd", *cloud_hull, false);


  double z_min = 0.01, z_max = 1; // we want the points above the plane, no farther than 5 cm from the surface
  pcl::PointIndices::Ptr output (new pcl::PointIndices);
  if (chull.getDimension () == 2)
  {
    pcl::ExtractPolygonalPrismData<pcl::PointXYZ> prism;
    prism.setInputCloud (cloud); //we want to do the convex hull fo the plane, so the input loud is the plane
    prism.setInputPlanarHull (cloud_hull);
    prism.setHeightLimits (z_min, z_max);
    prism.segment (*output);
  }
  else
   PCL_ERROR ("The input cloud does not represent a planar surface.\n");

  extract.setIndices (output);
  //extract.setNegative (false); //Removes part_of_cloud but retain the original full_cloud
  //extract.setNegative (true); // Removes all the points with index specified by indices from the point cloud "cloud"
  pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_filt (new pcl::PointCloud<pcl::PointXYZ>);
  extract.filter (*cloud_filt); 

  boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer (new pcl::visualization::PCLVisualizer ("3D Viewer"));
  viewer->setBackgroundColor (0, 0, 0);
 
  viewer->addPointCloud (cloud_filt, "voxel centroids");
  viewer->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE,2.0, "voxel centroids");
  viewer->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_OPACITY,0.95, "voxel centroids");

  while (!viewer->wasStopped ())
  {
    viewer->spinOnce (100);
  }

  return (0);
}