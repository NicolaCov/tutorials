#include <pcl/ModelCoefficients.h>
#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>
#include <pcl/sample_consensus/method_types.h>
#include <pcl/sample_consensus/model_types.h>
#include <pcl/filters/passthrough.h>
#include <pcl/filters/project_inliers.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/surface/convex_hull.h>
 #include <pcl/visualization/pcl_visualizer.h>
#include <pcl/filters/extract_indices.h>


#include <pcl/segmentation/extract_polygonal_prism_data.h>

typedef pcl::PointXYZRGBA PointT;

void help()
{
   std::cout << "argv[1] = pcd to work with\n";
   std::cout << "argv[2] = distance treshold for RANSAC plane estimation -> suggested 0.025\n";
   std::cout << "argv[3] = threshold for the pass filter -> suggested 1.1\n";
   return;
}

int
 main (int argc, char** argv)
{

  double th_ransac;
  double pass_th = 1.2;
  if (argc < 2)
  {
  help();
    return 0;
  }
  else if(argc  < 3)
  {
     th_ransac = 0.01;
  }
  else
  {
     th_ransac = atof(argv[2]);
     if(argc > 2)
        pass_th = atof(argv[3]);
  }


  pcl::PointCloud<PointT>::Ptr cloud (new pcl::PointCloud<PointT>), 
                                      plane (new pcl::PointCloud<PointT>);

  pcl::console::print_highlight ("Loading point cloud...\n");
  if (pcl::io::loadPCDFile<PointT> (argv[1], *cloud))
  {
    pcl::console::print_error ("Error loading cloud file!\n");
    return (1);
  }
  std::cout << "pcd loaded\n";  // Build a filter to remove spurious NaNs
  
  pcl::PassThrough<PointT> pass;
  pass.setInputCloud (cloud);
  pass.setFilterFieldName ("z");
  pass.setFilterLimits (0, pass_th);
  pass.filter (*cloud);

  pcl::console::print_highlight ("Estimating plane...\n");
  pcl::ModelCoefficients::Ptr coefficients (new pcl::ModelCoefficients);
  pcl::PointIndices::Ptr inliers (new pcl::PointIndices);
  // Create the segmentation object
  pcl::SACSegmentation<PointT> seg;
  // Optional
  seg.setOptimizeCoefficients (true);
  // Mandatory
  seg.setModelType (pcl::SACMODEL_PLANE);
  seg.setMethodType (pcl::SAC_RANSAC);
  seg.setDistanceThreshold (th_ransac);

  seg.setInputCloud (cloud);
  seg.segment (*inliers, *coefficients);

  // Extract inliers from the input cloud
  pcl::console::print_highlight ("Subtracting plane...\n");
  pcl::ExtractIndices<PointT> extract ;
  extract.setInputCloud (cloud);
  extract.setIndices (inliers);
  //extract.setNegative (false); //Removes part_of_cloud but retain the original full_cloud
  extract.setNegative (true); // Removes all the points with index specified by indices from the point cloud "cloud"
  extract.filter (*cloud); 

  boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer (new pcl::visualization::PCLVisualizer ("3D Viewer"));
  viewer->setBackgroundColor (0, 0, 0);
 
  viewer->addPointCloud (cloud, "voxel centroids");
  viewer->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE,2.0, "voxel centroids");
  viewer->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_OPACITY,0.95, "voxel centroids");
  viewer->addCoordinateSystem (0.5);
  while (!viewer->wasStopped ())
  {
    viewer->spinOnce (100);
  }

  return (0);
}