cmake_minimum_required(VERSION 2.8 FATAL_ERROR)

project(plane_removal)

find_package(PCL 1.7 REQUIRED)

include_directories(${PCL_INCLUDE_DIRS})
link_directories(${PCL_LIBRARY_DIRS})
add_definitions(${PCL_DEFINITIONS})

add_executable (convex_hull_plane src/convex_hull_plane.cpp)
target_link_libraries (convex_hull_plane ${PCL_LIBRARIES})
set_target_properties(convex_hull_plane PROPERTIES RUNTIME_OUTPUT_DIRECTORY ${PROJECT_SOURCE_DIR}/bin)

