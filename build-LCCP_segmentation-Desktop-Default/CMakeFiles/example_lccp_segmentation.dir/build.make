# CMAKE generated file: DO NOT EDIT!
# Generated by "Unix Makefiles" Generator, CMake Version 2.8

#=============================================================================
# Special targets provided by cmake.

# Disable implicit rules so canonical targets will work.
.SUFFIXES:

# Remove some rules from gmake that .SUFFIXES does not remove.
SUFFIXES =

.SUFFIXES: .hpux_make_needs_suffix_list

# Suppress display of executed commands.
$(VERBOSE).SILENT:

# A target that is always out of date.
cmake_force:
.PHONY : cmake_force

#=============================================================================
# Set environment variables for the build.

# The shell in which to execute make rules.
SHELL = /bin/sh

# The CMake executable.
CMAKE_COMMAND = /usr/bin/cmake

# The command to remove a file.
RM = /usr/bin/cmake -E remove -f

# Escaping for special characters.
EQUALS = =

# The top-level source directory on which CMake was run.
CMAKE_SOURCE_DIR = /home/ncovallero/Documents/Tutorials/LCCP_segmentation

# The top-level build directory on which CMake was run.
CMAKE_BINARY_DIR = /home/ncovallero/Documents/Tutorials/build-LCCP_segmentation-Desktop-Default

# Include any dependencies generated for this target.
include CMakeFiles/example_lccp_segmentation.dir/depend.make

# Include the progress variables for this target.
include CMakeFiles/example_lccp_segmentation.dir/progress.make

# Include the compile flags for this target's objects.
include CMakeFiles/example_lccp_segmentation.dir/flags.make

CMakeFiles/example_lccp_segmentation.dir/src/example_lccp_segmentation.cpp.o: CMakeFiles/example_lccp_segmentation.dir/flags.make
CMakeFiles/example_lccp_segmentation.dir/src/example_lccp_segmentation.cpp.o: /home/ncovallero/Documents/Tutorials/LCCP_segmentation/src/example_lccp_segmentation.cpp
	$(CMAKE_COMMAND) -E cmake_progress_report /home/ncovallero/Documents/Tutorials/build-LCCP_segmentation-Desktop-Default/CMakeFiles $(CMAKE_PROGRESS_1)
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Building CXX object CMakeFiles/example_lccp_segmentation.dir/src/example_lccp_segmentation.cpp.o"
	/usr/bin/c++   $(CXX_DEFINES) $(CXX_FLAGS) -o CMakeFiles/example_lccp_segmentation.dir/src/example_lccp_segmentation.cpp.o -c /home/ncovallero/Documents/Tutorials/LCCP_segmentation/src/example_lccp_segmentation.cpp

CMakeFiles/example_lccp_segmentation.dir/src/example_lccp_segmentation.cpp.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CXX source to CMakeFiles/example_lccp_segmentation.dir/src/example_lccp_segmentation.cpp.i"
	/usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -E /home/ncovallero/Documents/Tutorials/LCCP_segmentation/src/example_lccp_segmentation.cpp > CMakeFiles/example_lccp_segmentation.dir/src/example_lccp_segmentation.cpp.i

CMakeFiles/example_lccp_segmentation.dir/src/example_lccp_segmentation.cpp.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CXX source to assembly CMakeFiles/example_lccp_segmentation.dir/src/example_lccp_segmentation.cpp.s"
	/usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -S /home/ncovallero/Documents/Tutorials/LCCP_segmentation/src/example_lccp_segmentation.cpp -o CMakeFiles/example_lccp_segmentation.dir/src/example_lccp_segmentation.cpp.s

CMakeFiles/example_lccp_segmentation.dir/src/example_lccp_segmentation.cpp.o.requires:
.PHONY : CMakeFiles/example_lccp_segmentation.dir/src/example_lccp_segmentation.cpp.o.requires

CMakeFiles/example_lccp_segmentation.dir/src/example_lccp_segmentation.cpp.o.provides: CMakeFiles/example_lccp_segmentation.dir/src/example_lccp_segmentation.cpp.o.requires
	$(MAKE) -f CMakeFiles/example_lccp_segmentation.dir/build.make CMakeFiles/example_lccp_segmentation.dir/src/example_lccp_segmentation.cpp.o.provides.build
.PHONY : CMakeFiles/example_lccp_segmentation.dir/src/example_lccp_segmentation.cpp.o.provides

CMakeFiles/example_lccp_segmentation.dir/src/example_lccp_segmentation.cpp.o.provides.build: CMakeFiles/example_lccp_segmentation.dir/src/example_lccp_segmentation.cpp.o

CMakeFiles/example_lccp_segmentation.dir/src/removeTable.cpp.o: CMakeFiles/example_lccp_segmentation.dir/flags.make
CMakeFiles/example_lccp_segmentation.dir/src/removeTable.cpp.o: /home/ncovallero/Documents/Tutorials/LCCP_segmentation/src/removeTable.cpp
	$(CMAKE_COMMAND) -E cmake_progress_report /home/ncovallero/Documents/Tutorials/build-LCCP_segmentation-Desktop-Default/CMakeFiles $(CMAKE_PROGRESS_2)
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Building CXX object CMakeFiles/example_lccp_segmentation.dir/src/removeTable.cpp.o"
	/usr/bin/c++   $(CXX_DEFINES) $(CXX_FLAGS) -o CMakeFiles/example_lccp_segmentation.dir/src/removeTable.cpp.o -c /home/ncovallero/Documents/Tutorials/LCCP_segmentation/src/removeTable.cpp

CMakeFiles/example_lccp_segmentation.dir/src/removeTable.cpp.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CXX source to CMakeFiles/example_lccp_segmentation.dir/src/removeTable.cpp.i"
	/usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -E /home/ncovallero/Documents/Tutorials/LCCP_segmentation/src/removeTable.cpp > CMakeFiles/example_lccp_segmentation.dir/src/removeTable.cpp.i

CMakeFiles/example_lccp_segmentation.dir/src/removeTable.cpp.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CXX source to assembly CMakeFiles/example_lccp_segmentation.dir/src/removeTable.cpp.s"
	/usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -S /home/ncovallero/Documents/Tutorials/LCCP_segmentation/src/removeTable.cpp -o CMakeFiles/example_lccp_segmentation.dir/src/removeTable.cpp.s

CMakeFiles/example_lccp_segmentation.dir/src/removeTable.cpp.o.requires:
.PHONY : CMakeFiles/example_lccp_segmentation.dir/src/removeTable.cpp.o.requires

CMakeFiles/example_lccp_segmentation.dir/src/removeTable.cpp.o.provides: CMakeFiles/example_lccp_segmentation.dir/src/removeTable.cpp.o.requires
	$(MAKE) -f CMakeFiles/example_lccp_segmentation.dir/build.make CMakeFiles/example_lccp_segmentation.dir/src/removeTable.cpp.o.provides.build
.PHONY : CMakeFiles/example_lccp_segmentation.dir/src/removeTable.cpp.o.provides

CMakeFiles/example_lccp_segmentation.dir/src/removeTable.cpp.o.provides.build: CMakeFiles/example_lccp_segmentation.dir/src/removeTable.cpp.o

# Object files for target example_lccp_segmentation
example_lccp_segmentation_OBJECTS = \
"CMakeFiles/example_lccp_segmentation.dir/src/example_lccp_segmentation.cpp.o" \
"CMakeFiles/example_lccp_segmentation.dir/src/removeTable.cpp.o"

# External object files for target example_lccp_segmentation
example_lccp_segmentation_EXTERNAL_OBJECTS =

/home/ncovallero/Documents/Tutorials/LCCP_segmentation/bin/example_lccp_segmentation: CMakeFiles/example_lccp_segmentation.dir/src/example_lccp_segmentation.cpp.o
/home/ncovallero/Documents/Tutorials/LCCP_segmentation/bin/example_lccp_segmentation: CMakeFiles/example_lccp_segmentation.dir/src/removeTable.cpp.o
/home/ncovallero/Documents/Tutorials/LCCP_segmentation/bin/example_lccp_segmentation: CMakeFiles/example_lccp_segmentation.dir/build.make
/home/ncovallero/Documents/Tutorials/LCCP_segmentation/bin/example_lccp_segmentation: /usr/lib/x86_64-linux-gnu/libboost_system.so
/home/ncovallero/Documents/Tutorials/LCCP_segmentation/bin/example_lccp_segmentation: /usr/lib/x86_64-linux-gnu/libboost_filesystem.so
/home/ncovallero/Documents/Tutorials/LCCP_segmentation/bin/example_lccp_segmentation: /usr/lib/x86_64-linux-gnu/libboost_thread.so
/home/ncovallero/Documents/Tutorials/LCCP_segmentation/bin/example_lccp_segmentation: /usr/lib/x86_64-linux-gnu/libboost_date_time.so
/home/ncovallero/Documents/Tutorials/LCCP_segmentation/bin/example_lccp_segmentation: /usr/lib/x86_64-linux-gnu/libboost_iostreams.so
/home/ncovallero/Documents/Tutorials/LCCP_segmentation/bin/example_lccp_segmentation: /usr/lib/x86_64-linux-gnu/libboost_serialization.so
/home/ncovallero/Documents/Tutorials/LCCP_segmentation/bin/example_lccp_segmentation: /usr/lib/x86_64-linux-gnu/libpthread.so
/home/ncovallero/Documents/Tutorials/LCCP_segmentation/bin/example_lccp_segmentation: /usr/local/lib/libpcl_common.so
/home/ncovallero/Documents/Tutorials/LCCP_segmentation/bin/example_lccp_segmentation: /usr/lib/x86_64-linux-gnu/libflann_cpp_s.a
/home/ncovallero/Documents/Tutorials/LCCP_segmentation/bin/example_lccp_segmentation: /usr/local/lib/libpcl_kdtree.so
/home/ncovallero/Documents/Tutorials/LCCP_segmentation/bin/example_lccp_segmentation: /usr/local/lib/libpcl_octree.so
/home/ncovallero/Documents/Tutorials/LCCP_segmentation/bin/example_lccp_segmentation: /usr/local/lib/libpcl_search.so
/home/ncovallero/Documents/Tutorials/LCCP_segmentation/bin/example_lccp_segmentation: /usr/local/lib/libpcl_sample_consensus.so
/home/ncovallero/Documents/Tutorials/LCCP_segmentation/bin/example_lccp_segmentation: /usr/local/lib/libpcl_filters.so
/home/ncovallero/Documents/Tutorials/LCCP_segmentation/bin/example_lccp_segmentation: /usr/lib/libOpenNI.so
/home/ncovallero/Documents/Tutorials/LCCP_segmentation/bin/example_lccp_segmentation: /usr/lib/libvtkCommon.so.5.8.0
/home/ncovallero/Documents/Tutorials/LCCP_segmentation/bin/example_lccp_segmentation: /usr/lib/libvtkFiltering.so.5.8.0
/home/ncovallero/Documents/Tutorials/LCCP_segmentation/bin/example_lccp_segmentation: /usr/lib/libvtkImaging.so.5.8.0
/home/ncovallero/Documents/Tutorials/LCCP_segmentation/bin/example_lccp_segmentation: /usr/lib/libvtkGraphics.so.5.8.0
/home/ncovallero/Documents/Tutorials/LCCP_segmentation/bin/example_lccp_segmentation: /usr/lib/libvtkGenericFiltering.so.5.8.0
/home/ncovallero/Documents/Tutorials/LCCP_segmentation/bin/example_lccp_segmentation: /usr/lib/libvtkIO.so.5.8.0
/home/ncovallero/Documents/Tutorials/LCCP_segmentation/bin/example_lccp_segmentation: /usr/lib/libvtkRendering.so.5.8.0
/home/ncovallero/Documents/Tutorials/LCCP_segmentation/bin/example_lccp_segmentation: /usr/lib/libvtkVolumeRendering.so.5.8.0
/home/ncovallero/Documents/Tutorials/LCCP_segmentation/bin/example_lccp_segmentation: /usr/lib/libvtkHybrid.so.5.8.0
/home/ncovallero/Documents/Tutorials/LCCP_segmentation/bin/example_lccp_segmentation: /usr/lib/libvtkWidgets.so.5.8.0
/home/ncovallero/Documents/Tutorials/LCCP_segmentation/bin/example_lccp_segmentation: /usr/lib/libvtkParallel.so.5.8.0
/home/ncovallero/Documents/Tutorials/LCCP_segmentation/bin/example_lccp_segmentation: /usr/lib/libvtkInfovis.so.5.8.0
/home/ncovallero/Documents/Tutorials/LCCP_segmentation/bin/example_lccp_segmentation: /usr/lib/libvtkGeovis.so.5.8.0
/home/ncovallero/Documents/Tutorials/LCCP_segmentation/bin/example_lccp_segmentation: /usr/lib/libvtkViews.so.5.8.0
/home/ncovallero/Documents/Tutorials/LCCP_segmentation/bin/example_lccp_segmentation: /usr/lib/libvtkCharts.so.5.8.0
/home/ncovallero/Documents/Tutorials/LCCP_segmentation/bin/example_lccp_segmentation: /usr/local/lib/libpcl_io.so
/home/ncovallero/Documents/Tutorials/LCCP_segmentation/bin/example_lccp_segmentation: /usr/local/lib/libpcl_features.so
/home/ncovallero/Documents/Tutorials/LCCP_segmentation/bin/example_lccp_segmentation: /usr/local/lib/libpcl_visualization.so
/home/ncovallero/Documents/Tutorials/LCCP_segmentation/bin/example_lccp_segmentation: /usr/local/lib/libpcl_ml.so
/home/ncovallero/Documents/Tutorials/LCCP_segmentation/bin/example_lccp_segmentation: /usr/local/lib/libpcl_segmentation.so
/home/ncovallero/Documents/Tutorials/LCCP_segmentation/bin/example_lccp_segmentation: /usr/local/lib/libpcl_people.so
/home/ncovallero/Documents/Tutorials/LCCP_segmentation/bin/example_lccp_segmentation: /usr/local/lib/libpcl_registration.so
/home/ncovallero/Documents/Tutorials/LCCP_segmentation/bin/example_lccp_segmentation: /usr/local/lib/libpcl_keypoints.so
/home/ncovallero/Documents/Tutorials/LCCP_segmentation/bin/example_lccp_segmentation: /usr/local/lib/libpcl_outofcore.so
/home/ncovallero/Documents/Tutorials/LCCP_segmentation/bin/example_lccp_segmentation: /usr/local/lib/libpcl_stereo.so
/home/ncovallero/Documents/Tutorials/LCCP_segmentation/bin/example_lccp_segmentation: /usr/lib/x86_64-linux-gnu/libqhull.so
/home/ncovallero/Documents/Tutorials/LCCP_segmentation/bin/example_lccp_segmentation: /usr/local/lib/libpcl_surface.so
/home/ncovallero/Documents/Tutorials/LCCP_segmentation/bin/example_lccp_segmentation: /usr/local/lib/libpcl_recognition.so
/home/ncovallero/Documents/Tutorials/LCCP_segmentation/bin/example_lccp_segmentation: /usr/local/lib/libpcl_tracking.so
/home/ncovallero/Documents/Tutorials/LCCP_segmentation/bin/example_lccp_segmentation: /usr/lib/x86_64-linux-gnu/libboost_system.so
/home/ncovallero/Documents/Tutorials/LCCP_segmentation/bin/example_lccp_segmentation: /usr/lib/x86_64-linux-gnu/libboost_filesystem.so
/home/ncovallero/Documents/Tutorials/LCCP_segmentation/bin/example_lccp_segmentation: /usr/lib/x86_64-linux-gnu/libboost_thread.so
/home/ncovallero/Documents/Tutorials/LCCP_segmentation/bin/example_lccp_segmentation: /usr/lib/x86_64-linux-gnu/libboost_date_time.so
/home/ncovallero/Documents/Tutorials/LCCP_segmentation/bin/example_lccp_segmentation: /usr/lib/x86_64-linux-gnu/libboost_iostreams.so
/home/ncovallero/Documents/Tutorials/LCCP_segmentation/bin/example_lccp_segmentation: /usr/lib/x86_64-linux-gnu/libboost_serialization.so
/home/ncovallero/Documents/Tutorials/LCCP_segmentation/bin/example_lccp_segmentation: /usr/lib/x86_64-linux-gnu/libpthread.so
/home/ncovallero/Documents/Tutorials/LCCP_segmentation/bin/example_lccp_segmentation: /usr/lib/x86_64-linux-gnu/libqhull.so
/home/ncovallero/Documents/Tutorials/LCCP_segmentation/bin/example_lccp_segmentation: /usr/lib/libOpenNI.so
/home/ncovallero/Documents/Tutorials/LCCP_segmentation/bin/example_lccp_segmentation: /usr/lib/x86_64-linux-gnu/libflann_cpp_s.a
/home/ncovallero/Documents/Tutorials/LCCP_segmentation/bin/example_lccp_segmentation: /usr/lib/libvtkCommon.so.5.8.0
/home/ncovallero/Documents/Tutorials/LCCP_segmentation/bin/example_lccp_segmentation: /usr/lib/libvtkFiltering.so.5.8.0
/home/ncovallero/Documents/Tutorials/LCCP_segmentation/bin/example_lccp_segmentation: /usr/lib/libvtkImaging.so.5.8.0
/home/ncovallero/Documents/Tutorials/LCCP_segmentation/bin/example_lccp_segmentation: /usr/lib/libvtkGraphics.so.5.8.0
/home/ncovallero/Documents/Tutorials/LCCP_segmentation/bin/example_lccp_segmentation: /usr/lib/libvtkGenericFiltering.so.5.8.0
/home/ncovallero/Documents/Tutorials/LCCP_segmentation/bin/example_lccp_segmentation: /usr/lib/libvtkIO.so.5.8.0
/home/ncovallero/Documents/Tutorials/LCCP_segmentation/bin/example_lccp_segmentation: /usr/lib/libvtkRendering.so.5.8.0
/home/ncovallero/Documents/Tutorials/LCCP_segmentation/bin/example_lccp_segmentation: /usr/lib/libvtkVolumeRendering.so.5.8.0
/home/ncovallero/Documents/Tutorials/LCCP_segmentation/bin/example_lccp_segmentation: /usr/lib/libvtkHybrid.so.5.8.0
/home/ncovallero/Documents/Tutorials/LCCP_segmentation/bin/example_lccp_segmentation: /usr/lib/libvtkWidgets.so.5.8.0
/home/ncovallero/Documents/Tutorials/LCCP_segmentation/bin/example_lccp_segmentation: /usr/lib/libvtkParallel.so.5.8.0
/home/ncovallero/Documents/Tutorials/LCCP_segmentation/bin/example_lccp_segmentation: /usr/lib/libvtkInfovis.so.5.8.0
/home/ncovallero/Documents/Tutorials/LCCP_segmentation/bin/example_lccp_segmentation: /usr/lib/libvtkGeovis.so.5.8.0
/home/ncovallero/Documents/Tutorials/LCCP_segmentation/bin/example_lccp_segmentation: /usr/lib/libvtkViews.so.5.8.0
/home/ncovallero/Documents/Tutorials/LCCP_segmentation/bin/example_lccp_segmentation: /usr/lib/libvtkCharts.so.5.8.0
/home/ncovallero/Documents/Tutorials/LCCP_segmentation/bin/example_lccp_segmentation: /usr/lib/x86_64-linux-gnu/libopencv_videostab.so.2.4.8
/home/ncovallero/Documents/Tutorials/LCCP_segmentation/bin/example_lccp_segmentation: /usr/lib/x86_64-linux-gnu/libopencv_video.so.2.4.8
/home/ncovallero/Documents/Tutorials/LCCP_segmentation/bin/example_lccp_segmentation: /usr/lib/x86_64-linux-gnu/libopencv_ts.so.2.4.8
/home/ncovallero/Documents/Tutorials/LCCP_segmentation/bin/example_lccp_segmentation: /usr/lib/x86_64-linux-gnu/libopencv_superres.so.2.4.8
/home/ncovallero/Documents/Tutorials/LCCP_segmentation/bin/example_lccp_segmentation: /usr/lib/x86_64-linux-gnu/libopencv_stitching.so.2.4.8
/home/ncovallero/Documents/Tutorials/LCCP_segmentation/bin/example_lccp_segmentation: /usr/lib/x86_64-linux-gnu/libopencv_photo.so.2.4.8
/home/ncovallero/Documents/Tutorials/LCCP_segmentation/bin/example_lccp_segmentation: /usr/lib/x86_64-linux-gnu/libopencv_ocl.so.2.4.8
/home/ncovallero/Documents/Tutorials/LCCP_segmentation/bin/example_lccp_segmentation: /usr/lib/x86_64-linux-gnu/libopencv_objdetect.so.2.4.8
/home/ncovallero/Documents/Tutorials/LCCP_segmentation/bin/example_lccp_segmentation: /usr/lib/x86_64-linux-gnu/libopencv_ml.so.2.4.8
/home/ncovallero/Documents/Tutorials/LCCP_segmentation/bin/example_lccp_segmentation: /usr/lib/x86_64-linux-gnu/libopencv_legacy.so.2.4.8
/home/ncovallero/Documents/Tutorials/LCCP_segmentation/bin/example_lccp_segmentation: /usr/lib/x86_64-linux-gnu/libopencv_imgproc.so.2.4.8
/home/ncovallero/Documents/Tutorials/LCCP_segmentation/bin/example_lccp_segmentation: /usr/lib/x86_64-linux-gnu/libopencv_highgui.so.2.4.8
/home/ncovallero/Documents/Tutorials/LCCP_segmentation/bin/example_lccp_segmentation: /usr/lib/x86_64-linux-gnu/libopencv_gpu.so.2.4.8
/home/ncovallero/Documents/Tutorials/LCCP_segmentation/bin/example_lccp_segmentation: /usr/lib/x86_64-linux-gnu/libopencv_flann.so.2.4.8
/home/ncovallero/Documents/Tutorials/LCCP_segmentation/bin/example_lccp_segmentation: /usr/lib/x86_64-linux-gnu/libopencv_features2d.so.2.4.8
/home/ncovallero/Documents/Tutorials/LCCP_segmentation/bin/example_lccp_segmentation: /usr/lib/x86_64-linux-gnu/libopencv_core.so.2.4.8
/home/ncovallero/Documents/Tutorials/LCCP_segmentation/bin/example_lccp_segmentation: /usr/lib/x86_64-linux-gnu/libopencv_contrib.so.2.4.8
/home/ncovallero/Documents/Tutorials/LCCP_segmentation/bin/example_lccp_segmentation: /usr/lib/x86_64-linux-gnu/libopencv_calib3d.so.2.4.8
/home/ncovallero/Documents/Tutorials/LCCP_segmentation/bin/example_lccp_segmentation: /usr/local/lib/libpcl_common.so
/home/ncovallero/Documents/Tutorials/LCCP_segmentation/bin/example_lccp_segmentation: /usr/local/lib/libpcl_kdtree.so
/home/ncovallero/Documents/Tutorials/LCCP_segmentation/bin/example_lccp_segmentation: /usr/local/lib/libpcl_octree.so
/home/ncovallero/Documents/Tutorials/LCCP_segmentation/bin/example_lccp_segmentation: /usr/local/lib/libpcl_search.so
/home/ncovallero/Documents/Tutorials/LCCP_segmentation/bin/example_lccp_segmentation: /usr/local/lib/libpcl_sample_consensus.so
/home/ncovallero/Documents/Tutorials/LCCP_segmentation/bin/example_lccp_segmentation: /usr/local/lib/libpcl_filters.so
/home/ncovallero/Documents/Tutorials/LCCP_segmentation/bin/example_lccp_segmentation: /usr/local/lib/libpcl_io.so
/home/ncovallero/Documents/Tutorials/LCCP_segmentation/bin/example_lccp_segmentation: /usr/local/lib/libpcl_features.so
/home/ncovallero/Documents/Tutorials/LCCP_segmentation/bin/example_lccp_segmentation: /usr/local/lib/libpcl_visualization.so
/home/ncovallero/Documents/Tutorials/LCCP_segmentation/bin/example_lccp_segmentation: /usr/local/lib/libpcl_ml.so
/home/ncovallero/Documents/Tutorials/LCCP_segmentation/bin/example_lccp_segmentation: /usr/local/lib/libpcl_segmentation.so
/home/ncovallero/Documents/Tutorials/LCCP_segmentation/bin/example_lccp_segmentation: /usr/local/lib/libpcl_people.so
/home/ncovallero/Documents/Tutorials/LCCP_segmentation/bin/example_lccp_segmentation: /usr/local/lib/libpcl_registration.so
/home/ncovallero/Documents/Tutorials/LCCP_segmentation/bin/example_lccp_segmentation: /usr/local/lib/libpcl_keypoints.so
/home/ncovallero/Documents/Tutorials/LCCP_segmentation/bin/example_lccp_segmentation: /usr/local/lib/libpcl_outofcore.so
/home/ncovallero/Documents/Tutorials/LCCP_segmentation/bin/example_lccp_segmentation: /usr/local/lib/libpcl_stereo.so
/home/ncovallero/Documents/Tutorials/LCCP_segmentation/bin/example_lccp_segmentation: /usr/local/lib/libpcl_surface.so
/home/ncovallero/Documents/Tutorials/LCCP_segmentation/bin/example_lccp_segmentation: /usr/local/lib/libpcl_recognition.so
/home/ncovallero/Documents/Tutorials/LCCP_segmentation/bin/example_lccp_segmentation: /usr/local/lib/libpcl_tracking.so
/home/ncovallero/Documents/Tutorials/LCCP_segmentation/bin/example_lccp_segmentation: /usr/lib/libvtkViews.so.5.8.0
/home/ncovallero/Documents/Tutorials/LCCP_segmentation/bin/example_lccp_segmentation: /usr/lib/libvtkInfovis.so.5.8.0
/home/ncovallero/Documents/Tutorials/LCCP_segmentation/bin/example_lccp_segmentation: /usr/lib/libvtkWidgets.so.5.8.0
/home/ncovallero/Documents/Tutorials/LCCP_segmentation/bin/example_lccp_segmentation: /usr/lib/libvtkVolumeRendering.so.5.8.0
/home/ncovallero/Documents/Tutorials/LCCP_segmentation/bin/example_lccp_segmentation: /usr/lib/libvtkHybrid.so.5.8.0
/home/ncovallero/Documents/Tutorials/LCCP_segmentation/bin/example_lccp_segmentation: /usr/lib/libvtkParallel.so.5.8.0
/home/ncovallero/Documents/Tutorials/LCCP_segmentation/bin/example_lccp_segmentation: /usr/lib/libvtkRendering.so.5.8.0
/home/ncovallero/Documents/Tutorials/LCCP_segmentation/bin/example_lccp_segmentation: /usr/lib/libvtkImaging.so.5.8.0
/home/ncovallero/Documents/Tutorials/LCCP_segmentation/bin/example_lccp_segmentation: /usr/lib/libvtkGraphics.so.5.8.0
/home/ncovallero/Documents/Tutorials/LCCP_segmentation/bin/example_lccp_segmentation: /usr/lib/libvtkIO.so.5.8.0
/home/ncovallero/Documents/Tutorials/LCCP_segmentation/bin/example_lccp_segmentation: /usr/lib/libvtkFiltering.so.5.8.0
/home/ncovallero/Documents/Tutorials/LCCP_segmentation/bin/example_lccp_segmentation: /usr/lib/libvtkCommon.so.5.8.0
/home/ncovallero/Documents/Tutorials/LCCP_segmentation/bin/example_lccp_segmentation: /usr/lib/libvtksys.so.5.8.0
/home/ncovallero/Documents/Tutorials/LCCP_segmentation/bin/example_lccp_segmentation: /usr/lib/x86_64-linux-gnu/libopencv_photo.so.2.4.8
/home/ncovallero/Documents/Tutorials/LCCP_segmentation/bin/example_lccp_segmentation: /usr/lib/x86_64-linux-gnu/libopencv_legacy.so.2.4.8
/home/ncovallero/Documents/Tutorials/LCCP_segmentation/bin/example_lccp_segmentation: /usr/lib/x86_64-linux-gnu/libopencv_video.so.2.4.8
/home/ncovallero/Documents/Tutorials/LCCP_segmentation/bin/example_lccp_segmentation: /usr/lib/x86_64-linux-gnu/libopencv_objdetect.so.2.4.8
/home/ncovallero/Documents/Tutorials/LCCP_segmentation/bin/example_lccp_segmentation: /usr/lib/x86_64-linux-gnu/libopencv_ml.so.2.4.8
/home/ncovallero/Documents/Tutorials/LCCP_segmentation/bin/example_lccp_segmentation: /usr/lib/x86_64-linux-gnu/libopencv_calib3d.so.2.4.8
/home/ncovallero/Documents/Tutorials/LCCP_segmentation/bin/example_lccp_segmentation: /usr/lib/x86_64-linux-gnu/libopencv_features2d.so.2.4.8
/home/ncovallero/Documents/Tutorials/LCCP_segmentation/bin/example_lccp_segmentation: /usr/lib/x86_64-linux-gnu/libopencv_highgui.so.2.4.8
/home/ncovallero/Documents/Tutorials/LCCP_segmentation/bin/example_lccp_segmentation: /usr/lib/x86_64-linux-gnu/libopencv_imgproc.so.2.4.8
/home/ncovallero/Documents/Tutorials/LCCP_segmentation/bin/example_lccp_segmentation: /usr/lib/x86_64-linux-gnu/libopencv_flann.so.2.4.8
/home/ncovallero/Documents/Tutorials/LCCP_segmentation/bin/example_lccp_segmentation: /usr/lib/x86_64-linux-gnu/libopencv_core.so.2.4.8
/home/ncovallero/Documents/Tutorials/LCCP_segmentation/bin/example_lccp_segmentation: CMakeFiles/example_lccp_segmentation.dir/link.txt
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --red --bold "Linking CXX executable /home/ncovallero/Documents/Tutorials/LCCP_segmentation/bin/example_lccp_segmentation"
	$(CMAKE_COMMAND) -E cmake_link_script CMakeFiles/example_lccp_segmentation.dir/link.txt --verbose=$(VERBOSE)

# Rule to build all files generated by this target.
CMakeFiles/example_lccp_segmentation.dir/build: /home/ncovallero/Documents/Tutorials/LCCP_segmentation/bin/example_lccp_segmentation
.PHONY : CMakeFiles/example_lccp_segmentation.dir/build

CMakeFiles/example_lccp_segmentation.dir/requires: CMakeFiles/example_lccp_segmentation.dir/src/example_lccp_segmentation.cpp.o.requires
CMakeFiles/example_lccp_segmentation.dir/requires: CMakeFiles/example_lccp_segmentation.dir/src/removeTable.cpp.o.requires
.PHONY : CMakeFiles/example_lccp_segmentation.dir/requires

CMakeFiles/example_lccp_segmentation.dir/clean:
	$(CMAKE_COMMAND) -P CMakeFiles/example_lccp_segmentation.dir/cmake_clean.cmake
.PHONY : CMakeFiles/example_lccp_segmentation.dir/clean

CMakeFiles/example_lccp_segmentation.dir/depend:
	cd /home/ncovallero/Documents/Tutorials/build-LCCP_segmentation-Desktop-Default && $(CMAKE_COMMAND) -E cmake_depends "Unix Makefiles" /home/ncovallero/Documents/Tutorials/LCCP_segmentation /home/ncovallero/Documents/Tutorials/LCCP_segmentation /home/ncovallero/Documents/Tutorials/build-LCCP_segmentation-Desktop-Default /home/ncovallero/Documents/Tutorials/build-LCCP_segmentation-Desktop-Default /home/ncovallero/Documents/Tutorials/build-LCCP_segmentation-Desktop-Default/CMakeFiles/example_lccp_segmentation.dir/DependInfo.cmake --color=$(COLOR)
.PHONY : CMakeFiles/example_lccp_segmentation.dir/depend

