#include <iostream>
#include <pcl/ModelCoefficients.h>
#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>
#include <pcl/sample_consensus/method_types.h>
#include <pcl/sample_consensus/model_types.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/common/common.h>

bool next = false; //to go to the next step

// Types
typedef pcl::PointXYZRGBA PointT;

void help()
{
   std::cout << "argv[1] = pcd to work with\n";
   std::cout << "argv[2] = distance treshold for RANSAC plane estimation\n";
   return;
}

void keyboardEventOccurred (const pcl::visualization::KeyboardEvent &event,
                            void* viewer_void)
{
  boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer = *static_cast<boost::shared_ptr<pcl::visualization::PCLVisualizer> *> (viewer_void);
  if (event.getKeySym () == "n" && event.keyDown ())
  {
    std::cout << "n was pressed => next step" << std::endl;
    viewer->removePointCloud("Original");
    next = true;
  }
}

int
 main (int argc, char** argv)
{

  double th_ransac;
  if (argc < 2)
  {
	help();
  	return 0;
  }
  else if(argc  < 3)
     th_ransac = 0.01;
  else
     th_ransac = atof(argv[2]);
  

  pcl::PointCloud<PointT>::Ptr cloud(new pcl::PointCloud<PointT>);
  pcl::console::print_highlight ("Loading point cloud...\n");
  if (pcl::io::loadPCDFile<PointT> (argv[1], *cloud))
  {
    pcl::console::print_error ("Error loading cloud file!\n");
    return (1);
  }
  std::cout << "pcd loaded  " << cloud->points.size() <<"points\n";

  //visualize original point cloud
  boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer (new pcl::visualization::PCLVisualizer ("Original Point cloud"));
  viewer->setBackgroundColor (0, 0, 0);
  pcl::visualization::PointCloudColorHandlerRGBField<pcl::PointXYZRGBA> rgb_cloud_obj(cloud);
  viewer->addPointCloud<pcl::PointXYZRGBA> (cloud, rgb_cloud_obj, "Original");
  viewer->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE,2.0, "Original");
  viewer->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_OPACITY,0.95, "Original");
  viewer->addCoordinateSystem (1.0);
  viewer->initCameraParameters ();
  while (!viewer->wasStopped() && !next)
  {
    viewer->spinOnce (100);
    viewer->registerKeyboardCallback (keyboardEventOccurred, (void*)&viewer);
  }
  next = false;

  pcl::console::print_highlight ("Estimating plane...\n");
  pcl::ModelCoefficients::Ptr coefficients (new pcl::ModelCoefficients);
  pcl::PointIndices::Ptr inliers (new pcl::PointIndices);
  // Create the segmentation object
  pcl::SACSegmentation<PointT> seg;
  // Optional
  seg.setOptimizeCoefficients (true);
  // Mandatory
  seg.setModelType (pcl::SACMODEL_PLANE);
  seg.setMethodType (pcl::SAC_RANSAC);
  seg.setDistanceThreshold (th_ransac);

  seg.setInputCloud (cloud);
  seg.segment (*inliers, *coefficients);

  if (inliers->indices.size () == 0)
  {
    PCL_ERROR ("Could not estimate a planar model for the given dataset.");
    return (-1);
  }

  //set color of inliers
  for (size_t i = 0; i < inliers->indices.size (); ++i)
  {
	cloud->points[inliers->indices[i]].r = 0; 
	cloud->points[inliers->indices[i]].g = 255; 
	cloud->points[inliers->indices[i]].b = 0; 
  }

  rgb_cloud_obj.setInputCloud(cloud);
  viewer->addPointCloud<pcl::PointXYZRGBA> (cloud, rgb_cloud_obj, "Original");
  viewer->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE,2.0, "Original");
  viewer->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_OPACITY,0.95, "Original");
  while (!viewer->wasStopped() && !next)
  {
    viewer->spinOnce (100);
    viewer->registerKeyboardCallback (keyboardEventOccurred, (void*)&viewer);
  }
  next = false;


  // Extract inliers from the input cloud
  pcl::console::print_highlight ("Subtracting plane...\n");
  pcl::ExtractIndices<PointT> extract ;
  extract.setInputCloud (cloud);
  extract.setIndices (inliers);
  //extract.setNegative (false); //Removes part_of_cloud but retain the original full_cloud
  extract.setNegative (true); // Removes all the points with index specified by indices from the point cloud "cloud"
  extract.filter (*cloud); 

  //visualize inliers
  rgb_cloud_obj.setInputCloud(cloud);
  viewer->addPointCloud<pcl::PointXYZRGBA> (cloud, rgb_cloud_obj, "Original");
  viewer->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE,2.0, "Original");
  viewer->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_OPACITY,0.95, "Original");
  while (!viewer->wasStopped ())
  {
    viewer->spinOnce (100);
  }

  //save in a pcd file
  pcl::io::savePCDFileASCII ("test_pcd.pcd", *cloud);
  std::cout << "Saved: test_pcd.pcd\n";

  return (0);
}
